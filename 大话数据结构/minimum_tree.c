// 最小生成树

#include <stdio.h>
#define TRUE 1
#define FALSE 0
#define MAXVEX 200
#define INFINITY 65535

typedef char VertexType;  // 顶点类型
typedef int EdgeType; // 权重数值类型---0,1

typedef struct {
    VertexType vexs[MAXVEX]; //顶点一维数组
    EdgeType arc[MAXVEX][MAXVEX]; // 邻接矩阵二维数组
    int numVertexs,numEdges; // 顶点数与边数
}MGraph;

/* prime(普利姆)算法 */
// 感觉循环差不多
void MiniSpanTree(MGraph G){
    int min,i,j,k;
    int adjvex[MAXVEX];// 保存顶点下标
    int lowcast[MAXVEX];// 保存相关顶点间边的权值
    lowcast[0] = 0; // 表示0点已经加入生成树
    adjvex[0] = 0; // 初始化第一个顶点下标为0
    for(i=1;i<G.numVertexs;i++){
        // 循环除0点以外的所有顶点
        lowcast[i] = G.arc[0][i];// 加入权值
        adjvex[i] = 0;
    }
    for(i=1;i<G.numVertexs;i++){
        min = INFINITY;
        j=1;k=0;
        while(j < G.numVertexs){
            if(lowcast[j] != 0 && lowcast[j] < min){
                // 也就是说j下标顶点不参与，找最小的权重
                min = lowcast[j];
                k = j;// k储存最小权重下标的顶点
            }
            j++;
        }
        printf("(%d,%d)",adjvex[k],k);// 打印最小权值的边
        lowcast[k] = 0;// k加入最小生成树
        for(j = 1;j<G.numVertexs;j++){
            if(lowcast[j] != 0 &&G.arc[k][j] < lowcast[j]){
                lowcast[j] = G.arc[k][j];
                adjvex[j] = k;
            }
        }
    }
}

/* 克鲁斯卡尔（Kruskal）算法 */
// 需要用到边集数组
// 从边的角度出发
typedef struct{
    int begin;
    int end;
    int weight;
}Edge;
void MiniSpanTree_Kruskal(MGraph G){
    int i,n,m;
    Edge edges[MAXVEX]; // 边集数组,按照顺序排列
    int parent[MAXVEX]; // 判断是否形成了回路
    for(i=0;i<G.numVertexs;i++){
        parent[i] = 0;
    }
    for(i=0;i<G.numEdges;i++){
        n = find(parent,edges[i].begin);
        m = find(parent,edges[i].end);
        if(n != m){
            parent[n] = m;
            printf("(%d,%d),%d",edges[i].begin,edges[i].end,edges[i].weight);
        }
    }
}

int find(int *parent,int f){
    if(parent[f] > 0)
        f = parent[f];
    return f;
}
// 这个代码很是简洁