// 图研究顶点与顶点之间的关系
// 无向图没方向()，有向图有方向<>
// 密密麻麻
// 始终关注顶点与弧的数量

/* 邻接矩阵 */
// 一位数组表示顶点集合，二维数组表示边的集合
// 注意无向图与有向图很大的地方的不一样
// 图的权值当表示没有时用无穷大表示

#include<stdio.h>
typedef char VertexType;  // 顶点类型
typedef int EdgeType; // 权重数值类型
#define MAXVEX 100 // 最大顶点数
#define INFINITY 65535 // 表示无穷大的值

typedef struct {
    VertexType vexs[MAXVEX]; //顶点一维数组
    EdgeType arc[MAXVEX][MAXVEX]; // 邻接矩阵二维数组
    int numVertexs,numEdges; // 顶点数与边数
}MGraph;

/* 构造无向网图的临界矩阵 */
// 给边与顶点输入数据的过程
void CreatGraph(MGraph *G){
    int i,j,k,w;
    printf("请输入顶点数与边数：");
    scanf("%d" "%d",&G->numVertexs,&G->numEdges); //输入顶点数与边数
    for(i=0;i<G->numVertexs;i++){
        scanf(&G->vexs[i]); // 读入顶点信息，建立顶点表
    }

    for(i=0;i<G->numVertexs;i++){
        for(j=0;j<G->numVertexs;j++){
            G->arc[i][j] = INFINITY; //初始化
        }
    }

    for(k=0;k<G->numEdges;k++){
        printf("输入边（vi，vj）的上标与下标，和权重w：");
        scanf("%d" "%d" "%d",&i,&j,&w);
        G->arc[i][j] = w;
        G->arc[j][i] = G->arc[i][j]; // 无向图矩阵是对称的
    }
}
// 邻接矩阵会浪费很大的空间
// 所以下面采用图与链表相结合的方式

/* 邻接表 */
// 有向图与无向图有所不同 
// 结构定义

typedef struct EdgeNode{
    // 边表结点
    EdgeType weight;
    int adjvex; // 邻接点
    struct EdgeNode *next;
}EdgeNode;

typedef struct VertexNode{
    // 顶点域
    VertexType data;
    EdgeType *firstedge;
}VertexType,AdiList[MAXVEX];

typedef struct{
    AdiList adjlist;
    int numVertexes,numEdges;// 当前顶点数与边数
}Graphadjlist;


// 邻接表的建立
// 运用的是头插法，头指针指向新建立的元素
void CreatAdjlist(Graphadjlist *G){
    int i,j,k;
    EdgeNode *e;
    printf("请输入顶点数与边数：");
    scanf("%d" "%d",G->numVertexes,G->numEdges);
    for(i=0;i<G->numVertexes;i++){
        // 顶点表的填充
        scanf(&G->adjlist[i].data);
        G->adjlist[i].firstedge = NULL; // 将边表置为空集
    }
    for(k=0;k<G->numEdges;k++){
        printf("请输入（vi，vj）的顶点序号");
        scanf("%d,%d",&i,&j);
        e = (EdgeNode *)malloc(sizeof(EdgeNode));
        e->adjvex = j;
        e->next = G->adjlist[i].firstedge;
        G->adjlist[j].firstedge = e;

        e = (EdgeNode *)malloc(sizeof(EdgeNode));
        e->adjvex = i;
        e->next = G->adjlist[j].firstedge;
        G->adjlist[i].firstedge = e;
    }
}

/* 十字链表 */
// 同时整合邻接表与逆邻接表

// 邻接多重表
// 边集数组
