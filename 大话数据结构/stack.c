// 栈仅在表尾进行操作
// 后进先出（LIFO）
// 数组的首位为栈底

#include <stdio.h>
#define MAXSIZE 20
#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0

typedef int SElemType;
typedef int Status;

typedef struct {
    SElemType data[MAXSIZE];
    int top;// top 类指针
}SqStack;

// 基本操作

// 进栈操作
Status push(SqStack *S,int e){
    // 首先判断是否满栈
    if(S->top == MAXSIZE - 1){
        return ERROR;
    }
    
    S->top++; //上移
    S->data[S->top] = e;// 赋值
    return OK;
}

// 出栈操作
Status pop(SqStack *S,SElemType *e){
    // 先判断是否没了
    if(S->top < 0){
        return ERROR;
    }

    *e = S->data[S->top];
    S->top--;
    return OK;
}

// 一个数组有两个栈，栈共享空间
// 数组头尾都为栈底
// 当两个top相距为1时即为栈满
// 当top1 = -1 or top2 = n时即为栈空
// 注:相同类型

typedef struct {
    SElemType data[MAXSIZE];
    int top1;// 栈1
    int top2;// 栈2
}SqDoubleStack;

// 双栈的插入操作
Status push_D(SqDoubleStack *S,SElemType e,int label){
    // label 为判断进哪个栈的标志
    // 先判断是否满了
    if(S->top1 + 1 == S->top2)
        return ERROR;
    
    if(label == 1){
        S->top1++;
        S->data[S->top1] = e;
    }
    else{
        S->top2--;
        S->data[S->top2] = e;
    }

    return OK;
}

// 出栈操作
Status pop_D(SqDoubleStack *S,SElemType *e,int label){
    if(label == 1){
        // 先判断是否没得了
        if(S->top1 == -1)
            return ERROR;
        *e = S->data[S->top1];
        S->top1--;
    }
    else if(label == 2){
        // 同上
        if(S->top2 == MAXSIZE)
            return ERROR;
        *e = S->data[S->top2];
        S->top2++;
    }
    return OK;
}

// 栈的链式结构
// 可将头结点的指针作为栈顶
// 链栈也就失去了头指针,改为顶指针


// 节点设置
typedef struct stackNode{
    SElemType data;
    struct stackNode *next;
}stackNode,*LinkStackPtr;

// 栈的结构设置
typedef struct linkStack{
    LinkStackPtr top;// 头指针
    int count;// 链长
}linkStack;

// 基本操作
// 进栈操作
Status Push_S(linkStack *L,SElemType e){
    LinkStackPtr S;
    S = (LinkStackPtr)malloc(sizeof(stackNode));
    S->data = e;
    S->next = L->top;
    L->top = S;
    L->count++;
    return OK;
}

// 出栈操作
Status Pop_S(linkStack *L,SElemType *e){
    LinkStackPtr p;//保存要删除的节点
    // 要先判断是否为空
    p = L->top;
    *e = p->data;
    L->top = L->top->next;
    free(p);
    L->count--;
    return OK; 
}


