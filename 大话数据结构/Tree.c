#include<stdio.h>
// 基本树
#define MAXSIZE 50
#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0

typedef int Telemtypes;// 树的数据类型
typedef int Status;

// 树包括唯一根节点、中间结点、叶节点
// 这里用三种表示法构造树的结构
// 双亲表示法、孩子表示法、孩子兄弟表示法

/* 双亲表示法 */
// 不是链表，顺序结构
// 随意增减
typedef struct PTNode{
    Telemtypes data;
    int parent; //双亲的位置
}PTNode;

typedef struct {// 树的结构
    PTNode nodes[MAXSIZE]; // 将表头放到一维数组中
    int r,n;// 根的位置与结点数
}PTree;

/* 孩子表示法 */
// 这是链表
// 灵活设置
typedef struct CTNode{// 孩子结点
    int child;
    struct CTNode *next;
}*childptr;

typedef struct{// 表头结点
    Telemtypes data;
    childptr firstchild;
}CTBox;

typedef struct {// 树的结构
    CTBox node[MAXSIZE]; // 将表头放到一维数组中
    int r,n;// 根的位置与结点数
}CTree;

/* 孩子兄弟表示法 */
// 根结点的第一个孩子是确定的，右兄弟也是确定的
// 很像二叉树了

typedef struct CSNode{
    Telemtypes data;
    struct CSNode *firstchild,*rightsib;
}CSNode,*CSTree;


