// 查找
#include<stdio.h>
#define TRUE 1
#define FALSE 0

typedef int Status;
typedef struct{
    int data;
    struct BiTNode *lchild,*rchild;
}BiTNode,*BiTree;
/* 有序表的查找 */

/* 折半查找 */
int Half_search(int *a,int n,int key){
    // a为数组，n为数组大小，key为要查找的值
    int low,high,middle;
    int i;
    low = 0;
    high = n;
    while(low <= high){
        middle = (high + low)/2;
        if(key < a[middle]){
            high = middle - 1;
        }
        if(key > a[middle]){
            low = middle + 1;
        }
        else return middle;
    }
    return -1;
}

/* 插值查找 */
// 当数值分布比较均匀时
// 是改进middle的寻找办法，其它与折半查找基本相同
int Insert_search(int *a,int n,int key){
    // a为数组，n为数组大小，key为要查找的值
    int low,high,middle;
    int i;
    low = 0;
    high = n;
    while(low <= high){
        middle = low + (key - a[low]) * (high - low) / (a[high] - a[low]);
        if(key < a[middle]){
            high = middle - 1;
        }
        if(key > a[middle]){
            low = middle + 1;
        }
        else return middle;
    }
    return -1;
}

/* 斐波拉契查找 */
// 需要斐波那契数组F[]
int Fibonacci_search(int *a,int n,int key,int *F){
    int low,high,middle;
    int i,k;
    low = 0;
    high = 1;
    k = 0;
    while(n > F[k]){
        k++; // 计算n位于F数列的位置
    }
    for(i=n;i<F[k] - 1;i++){
        a[i] = a[n]; // 将多余的数值补全
    }

    // 开始循环
    while(low <= high){
        middle = low + F[k] - 1; 
        if(key < a[middle]){
            high = middle - 1;
            k - 1;
        }
        else if(key > a[middle]){
            low = middle + 1;
            k = k-2;
        }
        else{
            if(middle <= n) return middle;
            else return n;
        }
    }
    return 0;
}

/* 无序索引查找 */
// 稠密索引---类似记事本
// 分块索引---类似图书馆
// 倒排索引---关键码+记录表

/* 二叉排序树 */
// 动态
// 查找
Status BST_Search(BiTree T,int key,BiTree f,BiTree *p){
    // 递归
    // f指向T的双亲
    // 查找成功，P指向元素结点，返回true
    // 查找失败，p指向最后一个元素，返回false
    if(!T){
        // 查找不成功
        *p = f;
        return FALSE;
    }
    if(key == T->data){
        *p = T;
        return TRUE;
    }
    else if(key < T->data){
        return BST_Search(T->lchild,key,T,p);
    }
    else return BST_Search(T->rchild,key,T,p);
}

// 插入---在查找的基础之上
Status InsertBST(BiTree *T,int key){
    BiTree p,s;
    if(!BST_Search(*T,key,NULL,&p)){
        // 查找不成功
        s = (BiTree)malloc(sizeof(BiTNode));
        s->data = key;
        s->rchild = s->lchild = NULL;
        if(!p){
            // 为最后一个结点
            *T = s;
        }
        else if(key < p->data){
            p->lchild = s;
        }
        else p->rchild = s;
        return TRUE;
    }
    else return FALSE; // 有相同的
}

// 有了上面两个基础，构建二叉排序树就很快了
BiTree* CreatBST(int *a,int N){
    // a为数值数组,N为数组的大小
    int i;
    BiTree T;
    T = NULL;
    for(i=0;i<N;i++){
        InsertBST(&T,a[i]);
    }
    return T;
}

// 删除稍微麻烦点
// 需要考虑---叶子结点、只有一个子树的结点、有两个子树的结点
// 先查找
// 和前面的差别不大
Status DelectBSF(BiTree *T,int key){
    if(!T){
        return FALSE;
    }
    else{
        if(key == (*T)->data){
            Delect(T); // 找到数据了
        }
        else if(key < (*T)->data) return DelectBSF(&(*T)->lchild,key);
        else if(key > (*T)->data) return DelectBSF(&(*T)->rchild,key);
    }
}

Status Delect(BiTree *p){
    BiTree q,s;
    if((*p)->rchild == NULL){
        // 右空则只需要连接座结点
        q = *p;*p = (*p)->lchild;free(q);
    }
    else if((*p)->lchild == NULL){
        // 左空则只需要连接右结点
        q = *p;*p = (*p)->rchild;free(q);
    }
    else{ // 如果两个子树都不空
        q = *p;s = (*p)->lchild;
        // 找p的前驱
        while(s->rchild){
            q=s;s = s->rchild;
        }
        (*p)->data = s->data;
        if(q!=*p){
            q->rchild = s->lchild;
        }
        else 
            q->lchild = s->lchild;
        free(s);
    }
    return TRUE;
}


/* 平衡二叉树 */
// 左右子树的高度差不大于1，平衡因子绝对值小于等于1
// 最小不平衡树
// 在插入的过程中及时调整
// bf大于1就右旋，小于-1就左旋
typedef struct BINode{
    int data;
    int bf; // 平衡因子
    struct BINode *lchild,*rchild;
}BINode,*BITree;

// 右旋处理
void R_rotate(BITree *p){
    BITree l;
    l = (*p)->lchild;
    (*p)->lchild = l->rchild;
    l->rchild = (*p);
    (*p) = l;
}

// 左旋处理
void L_rotate(BITree *p){
    BiTree l;
    l = (*p)->rchild;
    (*p)->rchild = l->lchild;
    l->lchild = *p;
    (*p) = l;
}

#define LH 1 // 左高
#define EH 0
#define RH -1 // 右高
/* 算法结束，T指向新的根结点 */
void LeftBalance(BITree *T){
    BITree L,Lr;
    L = (*T)->lchild;
    switch(L->bf){
        // 检查左子树的平衡度
        case LH:// 新节点插在左孩子上，做单右旋处理
            (*T)->bf = L->bf = EH;
            R_rotate(T);
            break;
        case RH:// 新结点插入在左孩子的右子树上，做双旋处理
            Lr = L->rchild;
            switch(Lr->bf){
                case LH:
                    (*T)->bf = RH;
                    L->bf = EH;
                    break;
                case RH:
                    (*T)->bf = EH;
                    L->bf = LH;
                    break;
                case EH:
                    (*T)->bf = L->bf = EH;
                    break;
            }
            Lr->bf = EH;
            L_rotate(&(*T)->lchild);// 对T的左子树左旋处理
            R_rotate(T);// 
    }
}
// 右旋与左旋相当
void RightBalance(BITree *T){}
// 主函数了
// 插入一个就检查一下
// 左边检查了再右边检查
Status InsertAVL(BITree *T,int e,Status *taller){
    // taller反应T是否长高
    if(!*T){
        // 没有找到相同的数据，则创建
        (*T) = (BITree)malloc(sizeof(BINode));
        (*T)->data = e;
        (*T)->lchild = (*T)->rchild = NULL;
        (*T)->bf = EH;
        *taller = TRUE;
    }
    else{
        if(e == (*T)->data){
            // 找到了相同的数据
            *taller = FALSE;
            return FALSE;
        }
        if(e < (*T)->data){
            if(!InsertAVL(&(*T)->lchild,e,taller)){
                // 没有插入
                return FALSE;
            }
            if(*taller){
                // 对 T 的平衡性做处理
                switch((*T)->bf){
                    case LH:
                        LeftBalance(T);
                        *taller = FALSE;
                        break;
                    case EH:
                        (*T)->bf = LH;
                        *taller = TRUE;
                        break;
                    case RH:
                        (*T)->bf = EH;
                        *taller = FALSE;
                        break;
                }
            }
        }
        else{
            // 在右子树里面找
            if(!InsertAVL(&(*T)->rchild,e,taller)){
                return FALSE;
            }
            if(*taller){
                switch((*T)->bf){
                    case LH:
                        (*T)->bf = EH;
                        *taller = FALSE;
                        break;
                    case EH:
                        (*T)->bf = RH;
                        *taller = TRUE;
                        break;
                    case RH:
                        RightBalance(T);
                        *taller = FALSE;
                        break;
                }
            }
        }
    }
    return TRUE;
}
// 有了上面的代码，构建AVL就很简单了
