// 图的遍历
// 深度优先遍历
 
#include <stdio.h>
#define TRUE 1
#define FALSE 0
#define MAXVEX 20

typedef int Boolen;
Boolen visited[MAXVEX]; // 访问标志的数组
typedef char VertexType;  // 顶点类型
typedef int EdgeType; // 权重数值类型---0,1

typedef struct {
    VertexType vexs[MAXVEX]; //顶点一维数组
    EdgeType arc[MAXVEX][MAXVEX]; // 邻接矩阵二维数组
    int numVertexs,numEdges; // 顶点数与边数
}MGraph;


typedef struct EdgeNode{
    // 边表结点
    EdgeType weight;
    int adjvex; // 邻接点
    struct EdgeNode *next;
}EdgeNode;

typedef struct VertexNode{
    // 顶点域
    VertexType data;
    EdgeType *firstedge;
}VertexType,AdiList[MAXVEX];

typedef struct{
    AdiList adjlist;
    int numVertexes,numEdges;// 当前顶点数与边数
}Graphadjlist;

/* 邻接矩阵的深度优先遍历 */
// 可结合视频的观看
// 对顶点的访问操作
void DFS(MGraph G,int i){
    int j;
    visited[i] = TRUE;
    printf("%c",G.vexs[i]); // 对访问顶点的打印操作
    for(j=0;j<G.numVertexs;j++){
        if(G.arc[i][j] == 1 && !visited[j]){ // 边上有值
            DFS(G,j); // 对为访问的邻接顶点递归调用
        }
    }
}

// 遍历操作
void DFSTraverse(MGraph G){
    int i;
    // 对访问数组的初始化
    for(i=0;i<G.numVertexs;i++){
        visited[i] = FALSE;
    }
    for(i=0;i<G.numVertexs;i++){
        if(!visited[i])
            DFS(G,i);
    }
}

/* 邻接表的深度优化搜索 */
// 链表结构
void DFSlink(Graphadjlist GL,int i){
    EdgeNode *p;
    visited[i] = TRUE;
    printf("%c",GL.adjlist[i]); // 对访问的顶点进行操作
    p = GL.adjlist[i].firstedge; // 指向邻接点
    while(p){
        if(!visited[p->adjvex]){
            DFSlink(GL,p->adjvex);
        p = p->next;
        }
    }
}

void DFSlinkTraverse(Graphadjlist GL){
    int i;
    // 初始化
    for(i=0;i < GL.numVertexes;i++){
        visited[i] = FALSE;
    }
    for(i=0;i<GL.numVertexes;i++){// 连通图只会执行一次
        if(!visited[i])
            DFSlink(GL,i);
    }
}
/* 实例：马踏棋盘算法 */
// 规则：棋盘8*8，按照“马”的行走规则，要求每一个方格只能进入一次，要走完64格
// 用1-64来表示马行走的路径
// 详看test.c


/* 广度优先搜索 */
// 结合队列，相当于树的层次遍历
// 邻接矩阵的搜索
void BFSTraverse(MGraph G){
    int i,j;
    Queue Q; // Queue 可由数组模拟
    // 初始化
    for(i = 0;i <G.numVertexs;i++){
        visited[i] = FALSE;
    }
    InitQueue(&Q);// 初始化以队列
    for(i=0;i<G.numVertexs;i++){
        if(!visited[i]){
            visited[i] = TRUE;
            printf("%c",G.vexs[i]);// 对顶点的操作
            EnQueue(&Q,i);// 入队列
            while(!QueueEmpty(Q)){
                // 若队列不为空，也就是相邻的一层
                DeQueue(&Q,&i);// 出队列并赋值给i
                for(j=0;j<G.numVertexs;j++){
                    if(G.arc[i][j] == 1 && !visited[j]){
                        visited[j] = TRUE;
                        printf("%c",G.vexs[j]);
                        EnQueue(&Q,j);// 将新节点入队
                    }
                }
            }
        }
    }
}

// 邻接表的搜索
void BFSlinkTraverse(Graphadjlist GL){
    int i;
    EdgeNode *p;
    Queue Q;
     // 初始化
    for(i = 0;i <GL.numVertexes;i++){
        visited[i] = FALSE;
    }
    InitQueue(&Q);// 初始化以队列
    for(i=0;i<GL.numVertexes;i++){
        if(!visited[i]){
            visited[i] = TRUE;
            printf("%c",GL.adjlist[i].data);// 对顶点的操作
            EnQueue(&Q,i);// 入队列
            while(!QueueEmpty(Q)){
                p = GL.adjlist[i].firstedge; // p指向当前结点的邻接点
                while(p){
                    if(!visited[p->adjvex]){
                        visited[p->adjvex] = TRUE;
                        printf("%c",GL.adjlist[p->adjvex].data);
                        EnQueue(&Q,p->adjvex);// 将邻接点如对
                    }
                    p = p->next;// 指向下一邻接点   
                }
            }
        }
    }      
}

// 这个有点帅啊啊啊
// leetcode上骚操作不断