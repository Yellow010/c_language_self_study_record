#include<stdio.h>
// 线性表的演化
#define MAXSIZE 20
#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0

typedef int QElemType;
typedef int Status;
// 队列
// 一端插入，一端输出 -> 先进先出
// 尾部插入，头部输出

typedef struct{
    QElemType data[MAXSIZE];
    int front;// 头指针
    int rear;// 尾指针
}sqQueue;
// front 指向头部，rear指向尾部的下一个

// 基本操作
// 循环队列

/* 初始化空队列 */
Status InitQueue(sqQueue *Q){
    Q->front = 0;
    Q->rear = 0;
    return OK;
}

/* 循环队列求队列长度 */
Status QueueLength(sqQueue Q){
    return (Q.rear - Q.front - MAXSIZE) % MAXSIZE;
}

/* 入队列操作 */
Status insertQueue(sqQueue *Q,QElemType e){
    // 若队列未满则插入e作为队尾元素
    // 即先判断是否满了
    if((Q->rear + 1) % MAXSIZE == Q->front)// 下一位，有一个备用空间 
        return ERROR;
    Q->data[Q->rear] = e;
    // rear的位置需要注意，可能在front的前面
    Q->rear = (Q->rear + 1) % MAXSIZE;//公式
    return OK;
}

/* 出队列操作 */
Status DeQueue(sqQueue *Q,QElemType *e){
    // front为输出位置
    // 先判断是否空了
    if(Q->front == Q->rear)
        return ERROR;
    (*e) = Q->data[Q->front];
    Q->front = (Q->front + 1) % MAXSIZE;// 下一个位置
    return OK;
}

// 更好的结构
// 队列的链式结构

typedef struct QNode{
    QElemType data;
    struct QNode *next
}QNode,*Queueptr;

typedef struct{
    Queueptr front,rear;
}LinkQueue;

// 基本操作
// front指向头结点，rear指向最后一个元素，当为空时front=rear

/* 入队操作，尾插法更方便 */
Status QueueLinkInsert(LinkQueue *Q,QElemType e){
    Queueptr s;
    s = (Queueptr)malloc(sizeof(QNode));
    //赋值
    s->data = e;
    s->next = NULL;
    // 链接
    Q->rear->next = s;
    Q->rear = s;
    return OK;
}

/* 出队操作 */
// 需要把第一个给节点free
Status DeQueueLink(LinkQueue *Q,QElemType *e){
    Queueptr p;
    // 先判断是否为空
    if(Q->rear == Q->front)
        return ERROR;
    p = Q->front->next;
    (*e) = p->data;
    Q->front->next = p->next;
    // 判断尾巴是不是在删除的元素
    if(Q->rear == p)
        Q->rear = Q->front;
    free(p);
    return OK;
}



