#include<stdio.h>

#define MAXSIZE 50
#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0

typedef char Telemtypes;// 树的数据类型
typedef int Status;

/* 二叉树 Binary tree */
// 二叉树就每个结点最多两个叉叉
// 二叉树、满二叉树、斜二叉树、完全二叉树
// 二叉树的五个性质
// 因为二叉树孩子与双亲之间有较好的逻辑关系，就可以用顺序储存结构来表示
// 但注意顺序存储结构一般只适用于完全二叉树

// 二叉链表
// 包含左孩子右孩子与数据域，当然可以随意增添
typedef struct BINode{
    Telemtypes data;
    struct BINode *lchild,*rchild;
}BINode,*BITree;

// 基本操作
// 二叉树的遍历，递归的思想

/* 前序遍历 */
// 先遍历完左子树，再遍历右子树
void PreOrder(BITree T){
    if(T==NULL)
        return;
    // 访问结点的操作
    printf("%c",T->data);
    // 先遍历左子树
    PreOrder(T->lchild);
    // 再遍历右子树
    PreOrder(T->rchild);
}

/* 中序遍历 */
// 先从根结点开始（不是先访问），先访问左子树，再访问根结点，最后访问右子树
// 只是代码顺序的不同
void Inorder(BITree T){
    if(T == NULL)
        return ;
    Inorder(T->lchild);
    printf("%c",T->data);
    Inorder(T->rchild);
}

/* 后序遍历 */
// 从左到右先叶子后结点的方式遍历访问左右子树，最后根结点
// 同理于上面两步
void Postorder(BITree T){
    if(T == NULL)
        return ;
    Postorder(T->lchild);
    Postorder(T->rchild);
    printf("%c",T->data);
}

/* 层次遍历 */
// 从第一层，从左到右，逐层遍历


/* 二叉树的建立 */
// 扩展二叉树：把没有的点用虚结点填充
// 遍历的反向用法，把打印改为输入
// 这里用前序遍历
void CreatBinaryTree(BITree *T){
    Telemtypes ch;
    scanf("%c",&ch);
    if(ch == '#')
        *T = NULL;
    else{
        *T = (BITree)malloc(sizeof(BINode));
        if(!*T)
            exit(OVERFLOW);
        (*T)->data = ch;
        CreatBinaryTree((*T)->lchild);// 构造左子树
        CreatBinaryTree((*T)->rchild);// 构造右子树
    }

}
