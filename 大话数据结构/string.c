// 串串
// 一样两种存储方式，顺式与链式
// 数组可在第一个位置存下世纪字符串的长度
// 顺序存储结构在这里更好处理
// 重要操作：串的模式匹配

#include<stdio.h>

#define MAXSIZE 50
#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0

typedef int Status;
typedef char String[MAXSIZE+1];

// 基本操作一
// /* 串按顺序的朴素模式匹配 */
// 返回子串T在主串S中的第pos个字符之后的位置，若无则返回-1
// S[0],T[0]存储了字符串的长度
int Index(String T,String S,int pos){
    int i = pos;//i为主串S的下标，从pos位置开始
    int j;//j为子串T的下标
    while(i <= S[0] && j <= T[0]){//i小于S的长度,j小于T的长度
        // 假如匹配到了最后一组，S还剩下T[0]-1个，i最后会大于S[0]，停滞循环。
        // 开始比较
        if(S[i] == T[j]){
            i++;
            j++;
        }
        else{
            // i回到上次匹配的下一位
             i = i-j + 1;
             j=1;
        }
    }
    if(j > T[0]){
        return i-T[0];
    }
    else
        return -1;
}
// 过于繁琐且在一些情况下，会浪费一些时间

/* KMP模式匹配 */
// next数组的值为j的变化值
// 先找到next数组(灵魂)
void get_next(String T,int *next){
    int i,j;
    i=1;//i在一个周期中是不会减小的
    j=0;
    next[1] = 0;
    while(i < T[0]){//T[0]表示T的长度
        if(j==0 || T[i] == T[j]){//T[i]前缀,T[j]后缀
            i++;
            j++;
            next[i] = j;
        }
        else{
            j = next[j];
        }
    }
}

int Index_KMP(String S,String T,int pos){
    // 在主串中返回pos位置后第一个子串T的位置
    int i = pos;// 表示pos的位置

    int j=1;// 表示子串的下标位置

    int next[MAXSIZE];// j的next数组

    get_next(T,next);//得到next数组
    while(i < S[0] && j < T[0]){
        if( j == 0 || S[i] == T[j]){
            i++;
            j++;
        }
        else{
            j = next[j];//j回溯
        }
    }

    if(j > T[0])
        return i - T[0];
    else
        return -1;
}


/* KMP的算法改进 */
// 若子串前后两个字符相等可用前一个的next值取代后一个的
void get_nextval(String T,int *nextval){
    int i,j;
    i=1;//i在一个周期中是不会减小的
    j=0;
    nextval[1] = 0;
    while(i < T[0]){
        if(j==0 || T[i] == T[j]){
            i++;
            j++;
            if(T[i] != T[j]){// 前后不等
                nextval[i] = j;
            }
            else{//前后不等
                nextval[i] = nextval[j];
            }
        }
        else
            j = nextval[j];
    }
}