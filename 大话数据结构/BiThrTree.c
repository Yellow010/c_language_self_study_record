// 线索二叉树
// 相当于构建双向链表，将没有左右孩子的结点分配前驱指针与后驱指针
// 中途需要判断指针类型
// 而线索化是要对遍历的过程中修改空指针
#include <stdio.h>
#define MAXSIZE 20
#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0

typedef int Status;
typedef int TelemType;
typedef enum{Link,Thread} PointerTag;// Link表示左右孩子指针，thread表示前后驱指针

typedef struct BiThrNode{
    TelemType data;
    struct BiThrTree *leftchild,*rightchild;
    PointerTag ltag;
    PointerTag rtag;
}BiThrNode,*BiThrTree;


// 后序遍历的过程

//全局变量,指向刚刚访问过的结点
BiThrTree pre;
void InThreading(BiThrTree p){
    if(p){
        InThreading(p->leftchild);
        if(!p->leftchild){
            p->ltag = Thread; // 前驱标志
            p->leftchild = pre; // 指向前驱 
        }
        if(!pre->rightchild){ // 前驱没有右孩子
            pre->rtag = Thread; // 后驱线索
            pre->rightchild = p; // 前驱的后驱指向当前结点p
        }
        pre = p;
        InThreading(p->rightchild);
    }
}

// 添加线索二叉树的头结点
// 并完成简单遍历
Status InorderTraverse(BiThrTree T){
    // T指向头结点,头结点leftchild指向根结点
    BiThrTree p;
    p = T->leftchild;
    while(p != T){
        while(p->ltag == Link){
            p = p->leftchild;
            // 对结点的操作
            printf("%c",p->data);
        while(p->rightchild && p->rightchild != T){
            p = p->rightchild;
            printf("%c",p->data);
        }
        p = p->rightchild;
        }
    }
}
// 会有对结点的重复操作，不过问题不大