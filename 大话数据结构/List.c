/*Author :  Yellow010                  */
/*Propose:  线性表                     */
/* 配备《大话数据结构》                 */

// 零个或多个数据元素组成的有限序列
// 线性表->顺序存储结构（连续存储单元）->
// 顺序存储结构 -> 一维数组 
// 确定线性表的起始位置，长度，数据类型

#include<stdio.h>

#define MAXSIZE 20
#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0

typedef int Status;
typedef int ElemType;

typedef struct{
    ElemType data[MAXSIZE]; // 一维数组，类型确定
    int length;             // 长度
}Sqlist;

int main(){
    Sqlist List;
}
/* 基本操作*/
// 获得元素 getelem（）->确定序列、下标，返回函数
Status GetElem_s(Sqlist L,int i,ElemType *e){
    if(L.length == 0 || i < 1 || i > L.length){
        return ERROR;
    }
    *e = L.data[i-1];
    return OK;
}

// 插入元素 ListInsert() -> 判断异常、从后往前遍历移动元素
// i 表示第几个位置
Status ListInsert(Sqlist *L,int i,ElemType e){
    int j ;
    if(L->length > MAXSIZE) return ERROR;
    if(i < 1 || i > L->length+1) return ERROR;
    if(i < L->length){// 不在末尾
        for(j = L->length - 1;j >= i - 1;j--){
            L->data[j + 1] = L->data[j];
        }
    } 
    L->data[i] = e;
    L->length ++;
    return OK;
}

// 删除元素 ListDelect() -> 判断异常，从前往后遍历，减一
Status ListDelect(Sqlist *L,int i,ElemType *e){
    int j;
    if(i < 1 || i > L->length) return ERROR;
    if(L->length == 0) return ERROR;
    *e = L->data[i - 1];
    if( i < L->length){// 不是最后一个元素
        for(j = i-1 ; j < L->length;j++){
            L->data[j] = L->data[j + 1];
        }
    }
    L->length--;
    return OK;
}

// 链式结构
// 强调数据之间的逻辑联系
typedef struct Node{
    ElemType data;
    struct Node *next;
}Node;

typedef struct Node *LinkList;

/* 基本操作 */
// 获取函数 GetElem_l() -> 从第一个开始移动直到NULL
// 取出第i个位置
Status GetElem_l(LinkList L,int i,ElemType *e){
    int j;
    LinkList p;
    p = L->next;
    while( p && j < i){
        p = p->next;
        ++j;
    }
    if(!p || j > i){
        return ERROR;// 节点不纯在
    }
    *e = p->data;
    return OK;
}

// 插入 LinkInsert() -> 指向头结点、向后移动、替换语句
// 插入i节点之前
Status LinkInsert(LinkList *L,int i,ElemType e){
    LinkList p,s;
    int j=1;
    p = *L;
    while (p && j < i)
    {
        p = p->next;
        ++j;
    }
    if(!p || j > i) return ERROR;
    s = (LinkList)malloc(sizeof(Node));
    s->data = e;
    s->next = p->next;
    p->next = s;
    return OK;
}

// 删除函数 -> 指向头结点，遍历找到i位置，替换语句
Status LinkDelete(LinkList *L,int i,ElemType *e){
    LinkList q,p;
    int j=1;
    p = *L;
    while( p && j <i){
        p = p->next;
        ++j;
    }
    if(!(p->next) || j  > i) return ERROR;// 节点不存在
    q = p->next;
    *e = q->data;
    p->next = q->next;
    free(q);
    return OK;
}

// 整表创建
// 创建空头结点、新节点new_node随机赋值data，插入到头结点与前一新节点之间(从后往前)
void CreatList_1(LinkList *L,int n){
    LinkList new_node;
    int i;
    srand(time(0)); // 随机化种子
    *L = (LinkList)malloc(sizeof(Node));
    *L = NULL;
    for(i = 0;i < n;i++){
        new_node = (LinkList)malloc(sizeof(Node));
        new_node->data = rand() %100 + 1;
        new_node->next = (*L)->next;  
        (*L)->next = new_node;
    }
}

// 从前往后整表创建
// 需要设置尾部指针
void CreatList_2(LinkList *L,int n){
    LinkList new_node,r; // r为尾部指针
    int i;
    *L = (LinkList)malloc(sizeof(Node));
    r = *L;
    for(i = 0;i < n;i++){
        new_node = (LinkList)malloc(sizeof(Node));
        new_node->data = rand() % 100 + 1;
        r->next = new_node;
        r = new_node; // 重新回到尾巴
    }
    r->next = NULL;
}

// 整表删除
Status ClearLink(LinkList *L){
    LinkList p,q;
    p = (*L)->next;
    while(p){
        q = p->next;
        free(p);
        p = q;
    }
    (*L)->next = NULL;
    return OK;
}