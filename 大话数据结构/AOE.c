// 关键路径
// 路径上的最大长度表示关键路径
// 同层活动同时进行
// 最早开始时间与最晚开始时间是否相等，相等则为关键路径

#include<stdio.h>
#define MAXVEX 100
#define OK 1
#define ERROR 0

typedef int Status;

int *etv,*ltv;// 最早开始时间，最晚开始时间
int *Stack2;// 储存拓扑序列的栈
int top2;// 2栈的指针


// 先用拓扑结构寻找最早发生时间etv
// 不过会有所改动
typedef struct EdgeNode{
    int adjvex; // 储存顶点对应的下标
    int weight;
    struct EdgeNode *next;
}EdgeNode;

typedef struct VertexNode{ // 表头数组
    int in; // 顶点入度
    int data;
    EdgeNode *firstedge;
}VertexNode,AdjList[MAXVEX];

typedef struct{
    AdjList adjlist;
    int numVertexes,numEdges; // 顶点数与边数
}graphAdjlist,*GraphAdjList;

Status TopologicalSort(GraphAdjList GL){
    EdgeNode *e;
    int i,k,gettop;
    int top = 0;// 栈的指针
    int count = 0;// 统计输出点的个数
    int *Stack; // 储存入度为0的顶点
    Stack = (int *)malloc(sizeof(int) * GL->numVertexes);
    // 将入度为0的点入栈
    for(i=0;i<GL->numVertexes;i++){
        if(GL->adjlist[i].in == 0){
            Stack[top++] = i;
        }
    }
    
    top2 = 0;
    etv = (int *)malloc(sizeof(int) * GL->numVertexes);
    //先初始化0
    for(i=0;i<GL->numVertexes;i++){
        etv[i] = 0;
    }
    Stack2 = (int *)malloc(GL->numVertexes*sizeof(int));

    while(!top){
        // 出栈
        gettop = Stack[--top];
        printf("%d->",GL->adjlist[gettop].data);// 打印顶点
        count++;
        Stack2[++top2] = gettop; // 弹出的再压入栈2
        // 对此顶点弧表遍历
        for(e=GL->adjlist[gettop].firstedge;e;e->next){
            k = e->adjvex;
            if(!(--GL->adjlist[k].in)){
                // 为0则入栈
                Stack[++top] = k;
            }
            // 计算时间
            if((etv[gettop] + e->weight) > etv[k]){// 求各顶点发生的时间
                etv[k] = etv[gettop] + e->weight;
            }
        }
    }

    if(count < GL->numVertexes){// 如果为真则存在环
        return ERROR;
    }
    else return OK;
}

void CriticalPath(GraphAdjList GL){
    EdgeNode *e;
    int i,gettop,k,j;
    int ete,lte;
    TopologicalSort(GL); // 求拓扑结构
    ltv = (int *)malloc(sizeof(int) * GL->numVertexes);
    //初始化最晚时间
    for(i=0;i<GL->numVertexes;i++){
        ltv[i] = 0;
    }
    while(!top2){

        gettop = Stack2[top2--];
        for(e=GL->adjlist[gettop].firstedge;e;e->next){
            // 算ltv时间
            k = e->adjvex;
            if(ltv[k] - e->weight < ltv[gettop]){
                ltv[gettop] = ltv[k] - e->weight;
            }
        }

        for(j=0;j<GL->numVertexes;j++){
            for(e=GL->adjlist[gettop].firstedge;e;e->next){
                k = e->adjvex;
                ete = etv[j];
                lte = ltv[k]-e->weight;
                if(ete == lte){// 两者相等就是关键路径
                    printf("&d->%d length:%d",GL->adjlist[j].data,GL->adjlist[k].data,e->weight);
                }
            }
        }
    }
}