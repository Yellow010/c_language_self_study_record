// 拓扑结构
// 有向图，无连通
// 用邻接表与栈来实现
#include<stdio.h>
#define MAXVEX 100
#define OK 1
#define ERROR 0

typedef int Status;
typedef struct EdgeNode{
    int adjvex; // 储存顶点对应的下标
    int weight;
    struct EdgeNode *next;
}EdgeNode;

typedef struct VertexNode{ // 表头数组
    int in; // 顶点入度
    int data;
    EdgeNode *firstedge;
}VertexNode,AdjList[MAXVEX];

typedef struct{
    AdjList adjlist;
    int numVertexes,numEdges; // 顶点数与边数
}graphAdjlist,*GraphAdjList;

Status TopologicalSort(GraphAdjList GL){
    EdgeNode *e;
    int i,k,gettop;
    int top = 0;// 栈的指针
    int count = 0;// 统计输出点的个数
    int *Stack; // 储存入度为0的顶点
    Stack = (int *)malloc(sizeof(int) * GL->numVertexes);
    // 将入度为0的点入栈
    for(i=0;i<GL->numVertexes;i++){
        if(GL->adjlist[i].in == 0){
            Stack[top++] = i;
        }
    }
    
    while(!top){
        // 出栈
        gettop = Stack[--top];
        printf("%d->",GL->adjlist[gettop].data);// 打印顶点
        count++;
        // 对此顶点弧表遍历
        for(e=GL->adjlist[gettop].firstedge;e;e->next){
            k = e->adjvex;
            if(!(--GL->adjlist[k].in)){
                // 为0则入栈
                Stack[++top] = k;
            }
        }
    }

    if(count < GL->numVertexes){// 如果为真则存在环
        return ERROR;
    }
    else return OK;
}