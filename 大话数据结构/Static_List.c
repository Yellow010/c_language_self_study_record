/* 静态链表 */
// 用数组来描述的链表叫做静态链表
#include<stdio.h>

#define MAXSIZE 200
#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0

typedef int Status;
typedef int ElemType;
// LOOK AT ME
typedef struct{
    ElemType e;
    int cur;  //指向下一元素的指标，0表示无方向（空指针）
}Component,StaticLink[MAXSIZE];

// 第一个与最后一个元素做了特殊处理，都没有数值，最后一个元素指向第一个元素节点，相当于头结点。
// 初始化
Status InitList(StaticLink space){
    int i;
    for(i = 0; i < MAXSIZE-1 ;i++){
        space[i].cur = i+1;
    }
    space[MAXSIZE - 1].cur = 0; //头结点
    return OK;
}

// 插入元素，只需要改动cur就行，不过要先仿制链表的分配空间函数
Status malloc_SL(StaticLink space){
    int i;
    i = space[0].cur;
    if (space[0].cur)
        space[0].cur = space[i].cur;   // 备用链表向后移动一位
    return i;
}

// 在位置i之前插入元素
Status Insert_SL(StaticLink space,int i,ElemType e){
    int j,k,l;
    // 先获得起始元素的位置
    k = MAXSIZE - 1;
    if(i < 1 || i > ListLength(space) + 1)
        return ERROR;
    j = malloc_SL(space);
    if(j){
        for(l=0;l<i-1;l++){
            k = space[k].cur;// 找到位置
        }
        space[j].cur = space[k].cur; // 新元素cur赋值
        space[k].cur = j;
        return OK;
    }
    return ERROR;
}

// 删除操作
// 删i除i位置的元素
Status Delect_List(StaticLink L,int i){
    int j,k;
    if(i < 1||i > MAXSIZE - 1){
        return ERROR;
    }
    //先找元素的起始位置
    k = MAXSIZE - 1;
    // 找之前位置的元素
    for(j = 1;j < i-1;j++){
        k = L[k].cur;//徐徐渐进
    }
    //找到i的位置
    j = L[k].cur;
    L[k].cur = L[j].cur;
    free_SSL(L,j);
    return OK;
}

void free_SSL(StaticLink space,int k){
    //把第一个元素的cur指向删除的元素下标
    //k为删除元素的下标
    space[k].cur = space[0].cur;
    space[0].cur = k;
}
