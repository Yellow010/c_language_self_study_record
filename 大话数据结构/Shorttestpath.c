// 最短路径算法
// 起点与终点之间的权重之和最小


#include<stdio.h>
#define MAXVEX 100
#define INFINITY 65535
typedef int Patnarc[MAXVEX];// 储存最短路径下标的数组
typedef int ShortPathTable[MAXVEX];// 储存到各点的最短路径权值和

typedef char VertexType;  // 顶点类型
typedef int EdgeType; // 权重数值类型---0,1

typedef struct {
    VertexType vexs[MAXVEX]; //顶点一维数组
    EdgeType arc[MAXVEX][MAXVEX]; // 邻接矩阵二维数组
    int numVertexs,numEdges; // 顶点数与边数
}MGraph;

/* 迪杰斯特拉算法 */
// 正常思维
void ShorttestPath_Dijkstra(MGraph G,int v0,Patnarc *P,ShortPathTable *D){
    // p表示前驱结点下标，D（v）表示v0-v最短路径长度和
    int final[MAXVEX]; // final[w] = 1表示求得顶点v0-v1的最短路径
    int v,w,k,min;
    // 初始化
    for(v=0;v<G.numVertexs;v++){
        final[v] = 0;
        (*D)[v] = G.arc[v0][v];
        (*P)[v] = 0;
    }
    (*D)[v0] = 0;
    final[v0] = 1; // vo已经添加
    for(v=1;v<G.numVertexs;v++){
        min = INFINITY;
        for(w=0;w<G.numVertexs;w++){
           // 寻找v0最近的顶点
            if(!final[w] && (*D)[w] < min){
                k=w;
                min = (*D)[w];
            }
        }
        final[k] = 1;// 将最近的顶点置为1
        for(w=0;w<G.numVertexs;w++){
            // 修正最短路径与距离
            if(!final[w] && (min + G.arc[k][w] < (*D)[w])){
                (*D)[w] = min + G.arc[k][w];
                (*P)[w] = k;
            }
        }
    }
}
// 这是一个什么东西

// 精妙算法
/* 弗洛伊德算法 */
// 有中间变量
// 是一种比较
typedef int Patnmatric[MAXVEX][MAXVEX];// 储存最短路径下标的数组
typedef int ShortPathTable_F[MAXVEX][MAXVEX];// 储存到各点的最短路径权值和

typedef struct {
    VertexType vexs[MAXVEX]; //顶点一维数组
    EdgeType matrix[MAXVEX][MAXVEX]; // 邻接矩阵二维数组
    int numVertexs,numEdges; // 顶点数与边数
}MFGraph;

void ShortPath_F(MFGraph G,Patnmatric *P,ShortPathTable_F *D){
    int v,w,k;
    // 先初始化
    for(v=0;v<G.numVertexs;v++){
        for(w=0; w < G.numVertexs;w++){
            (*D)[v][w] = G.matrix[v][w];
            (*P)[v][w] = w;
        }
    }
    // 开始循环
    for(k=0;k<G.numVertexs;k++){
        for(v=0;v<G.numVertexs;v++){
            for(w=0;w<G.numVertexs;w++){
                if((*D)[v][w] > (*D)[v][k] + (*D)[k][w]){
                    (*D)[v][w] = (*D)[v][k] + (*D)[k][w];
                    (*P)[v][w] = (*P)[v][k];// 路径设置为经过下标为k的顶点
                }
            }
        }
    }
    // 这一步已经得到了(*P)与(*D)，所以可以通过（*P）得到最短路径
    for(v=0;v<G.numVertexs;v++){
        for(w = v+1;w<G.numVertexs;w++){
            printf("v%d-v%d , %d",v,w,D[v][w]);
            k = P[v][w];
            printf("path:%d",v);// 打印源点
            while(k!=w){
                printf("-> %d",k);// 打印中途顶点
                k = P[k][w];
            }
            printf("-> %d",w);// 打印终点
        }
    }
}
