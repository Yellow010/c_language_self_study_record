// 循环列表
// 尾指针不再指向空节点，而是头结点
// 这时候我们用尾指针来到导游
// 但是链表还是存在头指针

// 双向链表
// 结构体中还包含指向前一个数据的指针
// 头指针的前驱指向尾，尾指针的后驱指向头
// 插入与删除要谨慎

#include <stdio.h>
#define MAXSIZE 20
#define OK 1
#define ERROR 0
#define TRUE 1
#define FALSE 0

typedef int Status;
typedef int ElemType;

typedef struct Node{
    ElemType data;
    struct Node *next;
}Node;

typedef struct DulNode{
    ElemType data;
    struct DulNode *per;
    struct DulNode *next;
}DulNode,*DulNodeList;

typedef struct Node *LinkList;

int main(){

};

// 创建循环链表
Status circle_list(LinkList *L,int n){
    // n为个数
    int i;
    LinkList rear;// 尾指针
    LinkList new;//新节点
    (*L) = (LinkList)malloc(sizeof(Node));
    (*L)->next = NULL;
    rear = (*L)->next;

    srand(time(0)); //刷新时间
    for(i=0;i<n;i++){
        new = (LinkList)malloc(sizeof(Node));
        new->data = rand()%100+1; //100以内的随机数
        rear->next = new;// 尾插法
        rear = new;
    }

    rear->next = (*L);
    return OK;
}

// 创建双向链表
Status double_list(DulNodeList *L,int n){
    // n表示个数
    DulNodeList rear,temp,head;
    int i;
    (*L) = (DulNodeList)malloc(sizeof(DulNode));
    (*L)->next = NULL;
    (*L)->per = NULL;
    head = (*L);
    rear = (*L);

    srand(time(0));
    for (i = 0;i<n;i++){
        temp = (DulNodeList)malloc(sizeof(DulNode));
        temp -> data = rand()%20+1;

        rear->next = temp;//同样尾插法
        rear->next = temp;
        temp->per = head;
        head = temp;
        rear = temp;
    }

    (*L)->per = temp;
    rear->next = (*L);.

    retrun OK;
}

