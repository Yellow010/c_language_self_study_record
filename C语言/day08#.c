/*Author :  Yellow010                  */
/*Propose:  指针及其应用                */

#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#define SIZE 10
#define True 1
#define False 0
#define STACK_size 10
#define N 20

typedef int BOOL;
int content[STACK_size];
int *top = &content[0];

// 声明
void max_min(int a[],int n,int *max,int *min);
void split_time(long int total_sec,int *hr,int *min,int *sec);
void find_two_largest(int a[],int n,int *largest ,int *second_largest);
// 指针就是地址，指针变量就是存储地址的变量
// 指针变量的声明，前面加一个星号即可 int *p
// 指针变量指向相同数据类型
// &i为变量i的内存地址，*p为p的对象， p=&i，*p = i
// p的改变会影响 i
// 最早接触的scanf函数就用了指针
// const可以保护实际参数不受改变 void f(const int *p),*p不改变，p可以改变
// 指针也可以作为地址返回
// 指针在后面也可以指向函数

// 指针与数组
// 指针一定意义上代替了数组的下标 int a[10],*p; *p = &a[0] ; *p = 5 结果是a[0] = 5;
// 指针相加  p=p+3（数组下标加3） ，相减同理 ，指针相减为之间的距离的绝对值 i= p-q
// 指针之间可以比较，前提在同一数组，而且是位置大小的比较
// 指针的递进 *p++ = a[i++],++在*之前运行
// *p++,(*p)++,*p--,(*p)--
// 可以用数组名作为指向第一个元素的指针
// int a[10]; *a = 5 (a[0] = 5) ; *(a+1) = 10 (a[1] = 10);
// a++ 是错误的
// 用指针作为数组名 int a[10] , *p = a ; p[0] = a[0] ; p[1] = a[1];

// 指针处理多维数组：int *p ; for(p = &a[0][0]; p < a[Size - 1][Size - 1] ; p++);
// 处理某一行的元素：int *p,a[hang][lie],i;for(p = a[i];p < a[i] + lie;p++); a[i]是化简后的结果
// 解释：a[i] 本身就指向i行的第一个元素，而a[i] + lie等于&a[i][lie] ,p++行不变仅仅加的列
// 用二维数组名作为指针时 需要用两个*，**b

int main(){
    /*
    int i;
    int big,small;
    int a[SIZE];
    printf("Please Enter 10 Numbers:");
    for(i=0;i<SIZE;i++){
        scanf("%d",&a[i]);
    }
    max_min(a, SIZE , &big, &small);
    printf("The bigest is: %d\n",big);
    printf("The smallest is: %d\n",small);
    return 0;
    */

    /*
    long int sum;
    int hr,min,sec;
    srand((unsigned)time(NULL));// 重新播种
    sum = rand();
    printf("%d\n",sum);
    split_time(sum, &hr, &min, &sec);
    printf("The time is %d:%d:%d",hr,min,sec);
    return 0;
    */

    /*
    int a[SIZE],j;
    int largest,second_largest;
    printf("Please Enter: ");
    for(j=0;j < SIZE;j++){
        scanf("%d",&a[j]);
    }
    find_two_largest(a,SIZE,&largest,&second_largest);
    printf("Largest is:%d  Second_largest is:%d",largest,second_largest);
    */

   /* 数列反向改进版 
    int a[SIZE],*p;
    printf("Please Enter：");
    for(p = a;p < a + SIZE;p++){
        scanf("%d",p);
    }
    for(p = a + SIZE - 1;p >= a;p--){
        printf("  %d",*p);
    }
    return 0;
    */

   /* 倒叙字符串 */
   /*
   int i=0,j;
   char c,a[N];
   char *p = &a[0];
   printf("Please enter:");
   while((c = getchar()) != '\n' && i < N){
       *p++ = c;
       i++;
   }
   printf("The result is:");
   for(p = &a[i];p >= &a[0];p--){
       printf("%c",*p);
   }
   return 0;
   */

   /* 寻找特定温度 */
   
   int tem[7][24];
   int *S;
   int i,j,num;
   srand((unsigned) time(NULL));
   for(i = 0;i < 7;i++){
       for(S = tem[i] ; S < tem[i] + 24 ; S++){
           num = rand() % 40;
           *S = num;
       }
   }
   if( search(tem[0],7,24) )
       printf("True");
   else printf("False");
   return 0;
/*
   for(j = 0;j < 7;j++){
       for(S = tem[j] ; S < tem[j] + 24 ; S++){
            if(*S == 32)
               printf("(%d %d)",i,j);
       }
   }
   return 0;
   // 确实好用
*/
}

/* 找出数组中最大最小元素 */
void max_min(int a[],int n,int *max,int *min){
    *max = a[0];
    *min = a[0];
    int i;
    for(i=01;i<n;i++){
        if(a[i] > *max)
            *max = a[i];
        if(a[i] < *min)
            *min = a[i];
    }
// 注：这里*max 与 *min的变化会影响世纪实际参数。。。
}

/* 时间的等价表示 */
void split_time(long int total_sec,int *hr,int *min,int *sec){
    *min = total_sec / 60;
    *sec = total_sec - *min * 60;
    if(*min > 60){
        *hr = *min / 60;
        *min = *min - *hr * 60;
    }
}

/* 寻找最大与第二大元素 */
void find_two_largest(int a[],int n,int *largest ,int *second_largest){
    *largest = a[0];
    *second_largest = a[1];
    int i;
    for(i = 1;i < n;i++){
        if(a[i] > *largest){
            *second_largest = *largest;
            *largest = a[i];
        }
    }
}

/* 用指针噶爱写栈的结构 */
void make_empty(void){
    top = 0;
}

BOOL is_empty(void){
    return top == 0;
}

BOOL is_full(void){
    return top == STACK_size;
}

void push(char i){
    if(!is_full)
        *top++ = i;
}

int pop(void){
    if(!is_empty)
        return *--top;
}

// 寻找特定温度函数
BOOL search(int *a,int hang,int lie){
    int i;
    int *p;
    for(i = 0;i < hang;i++){
       for(p = a[i] ; p < a[i] + lie ; p++){
            if(*p == 32)
                return True;
       }
   }
}
