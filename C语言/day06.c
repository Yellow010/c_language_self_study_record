/*Author :  Yellow010                  */
/*Propose:  函数                       */
/*Date   : 2020/5/19                   */
/*Time   : 22:00-23：30                */

// 一下看不出来哪里有问题17题
#include<stdio.h>
#define True 1
#define False 0
#define N 10
typedef int BOOl;
void Quicksort(int a[],int low,int high);
int spilt(int a[],int low,int high);
int GCD(int m,int n);
void selection(int a[],int n);
void selection_sort(int n,int a[]);

int main(){
    int m,num,n;
    int a[N];
    /* 计算素数
    printf("Enter:");
    scanf("%d",&num);
    if(is_prime(num)){
        printf("Yes");
    }
    else printf("No");
    */

   // 函数声明，是对程序所要用的函数进行一个大体的浏览
   // 声明放在之前，如 float average（int a，int b）;可以忽略掉名字a、b
   // 遇到不正确的实际参数类型的传递，调用前先强制转换
   // 数组作为实际参数传递，如 int f（int a[]）,如果需要数组的长度，需要添加参数
   // 注意：在把数组名给函数时，不要加方括号 如 total(a,50);,数组长度可以去小于等于真实长度的任意值
   // 但当两维数组是就需要指出列的准确大小，，可以使用 “ 指针数组 ”解决这个问题
   // 终止函数 exit() from <stdlib.h>

   // 递归函数，我调用我自己

   // 经典快速排序
   /*
   printf("Enter:");
   scanf("%d %d %d %d %d %d %d %d %d %d",&a[0],&a[1],&a[2],&a[3],&a[4],&a[5],&a[6],&a[7],&a[8],&a[9]);
   Quicksort(a,0,N - 1);
   printf("\nIn sorted order:");
   for(n = 0;n < N;n++){
       printf("%d",a[n]);
   */

    // 最后一题17
    printf("Enter:");
    for (n = 0;n < N;n++){
        scanf("%d",&a[n]);
    }
    // selection(a,N);
    selection_sort(N,a);
    for(m = 0;m < N;m++){
        printf("%d",a[m]);
    }
    return 0;
}

// 计算平均值的函数
float average(float a,float b){
    return (a + b) / 2;
}

// 不代返回数的类型用 void 声明
// 也可以用到函数里面作为没有参数的占位符
// 函数无法返回数组

// 判定素数
BOOl is_prime(int n){
    int i;
    for(i = 2;i * i <=n ;i++){
        if(n % i == 0){
            return False;
        }
    }
    return True;
}

// 快速排序
void Quicksort(int a[],int low,int high){
    int middle;
    if(low >= high) return;
    middle =  spilt(a,low,high);
    Quicksort(a,low,middle - 1);
    Quicksort(a,middle + 1,high);
}

int spilt(int a[],int low,int high){
    int temp = a[low];
    for(;;){
        // 从右往左走
        while(low < high && temp <= a[high]){
            high --;
        }
        if(low >= high) break;
        a[low++] = a[high];

        // 从左往右走
        while(low < high && temp >= a[low]){
            low ++;
        }
        if(low >= high) break;
        a[high--] = a[low];
    }
    a[high] = temp;
    return high;
}

// 计算最大公约数
int GCD(int m,int n){
    int temp;

}

// 转换位置函数
/*辗转相除法
两整数a和b：
① a%b得余数c
② 若c=0，则b即为两数的最百大公约数，结束
③ 若c≠0，则a=b，b=c，再度回去执行①
*/

void selection(int a[],int n){
    int max,temp;
    int i,j;
    j=0;
    max = a[0];
    if(n > 0){
        for(i = 1;i < n;i++){
           if(a[i] > max){
              max = a[i];
              j = i;
        }
    }
       // 交换
        temp = a[n-1];
        a[n-1] = max;
        a[j] = temp;
    } 

    n--;
    if(n==0) return;
    selection(a,n);
}

void selection_sort (int n, int a[n]) {
	
	int i, j, max = a[0];
	
	for (i = 0; i < n; i++) {
		if (max <= a[i]) {
			max = a[i];
			j = i;
		}
	}
	a[j] = a[n-1];
	a[n - 1] = max;
	
	//函数每执行一次， 就会将数组的长度减一 
	n--;
	if (n == 0) return;
	selection_sort (n, a);
}



