/*Author :  Yellow010                  */
/*Propose: 基本概念，输入输出，表达式 
           选择语句                     */
/*Date   : 2020/4/22                   */
/*Time   : 22:00-23：30                */

#include<stdio.h>
/*
  算术表达式，关系表达式，逻辑表达式,复合表达式
  
*/
int main()
{
/*
  计算通用产品代码的校验位
  0 37000 00407 ？
  奇数位相加 * 3 + 偶数位相加 - 1 的结果除以10取余数，
  再用9减去上述余数得到校验码。
*/
/*
    int str,i1,i2,i3,i4,i5,i6,i7,i8,i9,i10;
    int sumQ,sumO;
    scanf("%1d",&str);
    scanf("%1d%1d%1d%1d%1d",&i1,&i2,&i3,&i4,&i5);
    scanf("%1d%1d%1d%1d%1d",&i6,&i7,&i8,&i9,&i10);

    sumQ = str + i2 + i4 + i6 + i8 + i10;
    sumO = i1 + i3 + i5 + i7 + i9;

    printf("效验码为：%d",9 - (3 * sumQ + sumO - 1) % 10);
    return 0;
*/

/* 数字反转
   int num,result;
   int a,b;
   scanf("%2d",&num);
   a = num / 10;
   b = num % 10;
   result = a + b * 10;
   printf("%d",result);
*/


}

/*
  强化练习 leetcode 第七题 ； 数字反转
  2^31-1=2147483647,-2^31=-2147483648
*/
/*
int reverse(int x){
  int pop,rev;
  int Max = 0x7fffffff,Min = 0x80000000;
  while(x != 0){
    pop = x % 10;
    x = x / 10;
    if(rev > Max / 10 || (rev = Max / 10 && pop > 7))return 0;
    if(rev < Min / 10 || (rev = Min / 10 && pop < -8))return 0;
    rev = rev * 10 + pop;
  }
  return rev;
}
*/
/*高手解法
int reverse(int x){
    long count=0;
    while(x!=0){
        count=count*10+x%10;
        x=x/10;
    }
    return count>2147483647||count<-2147483648?0:count;
}
*/