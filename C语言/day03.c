/*Author :  Yellow010                  */
/*Propose:  循环 (while,do,for)        */
/*Date   : 2020/5/14                   */
/*Time   : 22:00-23：30                */

#include<stdio.h>
int main()
{
    /* 显示平方值的表格 while */
    /* 1    1
       2    4
       3    9
       4    16
       5    25
    */
   /*
   int i,n;
   scanf("%d",&n);
   i = 1;
   while(i <= n){
       printf("%d\t%d\n",i,i * i);
       i++;
   }
   */

  /* 数列求和 */
  /*
   int n,sum = 0;
   scanf("%d", &n);
   while(n != 0)
   {
       sum = sum + n;
       scanf("%d", &n);
   }
    printf("sum: %d\n",sum);
   */
  
    /*计算整数中的数字位数，用do循环*/
    /*
    int n,i;
    scanf("%d", &n);
    i = 0;
    do{
        i++;
        n = n / 10;
    }while(n > 1);
    printf("数字位数为： %d位",i);
    */

    /* 显示平方值的表格 for */
    /*
    int i,n;
    scanf("%d", &n);
    for(i = 1;i <= n;i++){
        printf("%10d%10d\n",i,i *i);
    }
    return 0;
    */
   
    /*账目簿计算*/
    /*
    int m,n,y=500;
    for(;;){
        printf("Enter command: ");
        scanf("%d",&n);
        if (n == 4) break;
        switch(n){
            case 0:printf("\nclear:%d\n",y);break;
            case 1:printf("\ncredict:"),scanf("%d",&m),y += m;break;
            case 2:printf("\ndebit:"),scanf("%d",&m),y -= m;break;
            case 3:printf("\nbalance:%d\n",y);break;
        }
    }
    */

   /*practice 01*/
   /*
   float i,max,n;
   max = 0;
   for(;;){
       printf("Enter command:");
       scanf("%f",&n);
       if(n == 0) break;
       while(n > 0){
           if (n > max) max = n;
           break;
       }
   }
   printf("The last number is:%f",max);
   return 0;
   */
  
   /* 最大公约数 ,最简式*/
   /*
   int m,n,temp;
   printf("请输入：");
   scanf("%d,%d",&m,&n);
   GCD(m,n);
   printf("number is ：%d",n);
   */

   /* 编写日历 */
   /*
   int i,n,s,c;
   printf("请输入天数：");
   scanf("%d",&n);
   printf("\n请输入起始星期：");
   scanf("%d",&s);
   c = s;
   while(s != 1){
       printf("\t");
       s--;
   }
   for(i = 1;i<=n;i++,c++){
       if(c > 7){
           printf("\n");
           c = 1;
       }
       printf("%d\t",i);
   }
   return 0;
   */

}
