/*Author :  Yellow010                  */
/*Propose:  声明                       */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>

// 声明格式：声明说明符 声明符  
// 声明说明符有三种：4个存储类型，2个类型限定符，类型说明符
// 静态存储期限可以无限期保留变量的值
// 变量声明外部默认为静态，块内默认为auto
// auto类型只能作用于块内部，自动声明。
// 块外声明static变量可以作用于整个文件，其它文件不可使用；块内声明static变量，只初始化一次（auto每次调用都要初始化），可以返回指向static变量的指针
// extern 可以是几个源文件同时共享变量，extern int i = 0;
// register 要求将变量存储在寄存器中而不是内存中，值作用于块中，且是没有地址的（不可用&符号），变量在块中频繁使用时可用。
// 可以使用extern和static声明函数，默认为extern。如果确定函数不会再其他文件中使用时，精良用static声明。
// const声明的变量是只读的，只能使用它的值，不能改变
// 类型说明符的声明有 * 、[] 、()、随意组合如:int *p[10] -> 声明指针数组、int *p(float) ->声明函数实际参数为float型返回指向int值得指针
// int (*p)(int) ->声明指向函数的指针，函数实参为int，返回int型
// 考验：int *(*x[10])(int) 解释
// 可以利用类型定义来简化声明
// 具有静态存储期限的初始化必须是常量，auto则不需要。

int main(){
    return 0;
}