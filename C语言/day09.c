/*Author :  Yellow010                  */
/*Propose:  字符串及其应用              */
#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<string.h>
#include <ctype.h>

#define SIZE 10
// 如果字符串面量太长，可以使用  \ 来分段
// 更好的分割方法
// printf("You are"
//        "a girl\n" );
// n+1个空间 储存n个字符，最后一位为 \0
// 字符串储存在数组里面，允许使用char *指针来使用字符串面量
// char *p; p = 'abc'(p指向abc的第一个字符，等价于p = "abc"[0])
// "a" and 'a' 大不相同
// 声明字符串数组时记得把空字符也算上
// 初始化字符串数组 char str[8] = "June 23";也可以忽略8，会自动计算
// 指针的声明 char *str = "June 23",不可修改*str的值，若要修改
// char str[SIZE],*p; p = str;这是就可以修改了
// %s 允许printf写字符串 ， 也可以使用 "%.6s"来断句， 而用%ms来写可以在m区域内显示，并右对齐
// 方便的用puts（）函数来输出
// 用scanf函数来读 scanf("%s",a)不需要加 & 符号
// 方便可用gets（）函数来读入，会直接读入一整行
// 通常两个读入函数都是无法检测是否填满的，对于scanf可以用 "%ms"来控制读入m个，gets没有
// 用指针来访问数组更加方便。
// 直接将字符串“赋值到”数组中是不正确的 如： int str[10]；str = "asd" *wrong*
// 但初始化是正确的的 str[10] = "asd"
// C语言字符串库 #include<string.h>
// strcpy() 复制函数 char* strcpy(char *s1,const char *s2) 把字符串s2复制给字符串s1，函数返回s1,strcpy(&str[2],"clock");
// strcat() 字符串拼接函数 char* strcat(char *s1,const char *s2) 把字符串2的内容追加到1的末尾
// strcmp() 字符串比较 int strcmp(const char *s1,const char *s2) s1在前，s2在后 ，比较方式类似于字典，比较第一个不同符号的大小
// strlen() 字符串长度 size_t strlen(const char *s) size_t = unsigned (long) int;不包括空字符
// 惯用法 while(*s++) or while(*s) s++ ;while (*p++ = *s++) 没有错
// 字符串很多时候是参差不齐的就需要参差不齐的二维数组，将数组的所有元素变成指向字符串的指针就行 int *str[3]={"asdd","adaad","avf"}
// 但访问和普通二维数组一样
// 

int main(){
    /*
    char *p = "abc";
    puts(p);
    putchar(*p);
    return 0;
    */

   /*
   char str[SIZE] = "I love y_u";
   int i,j;
   //strcap(str);
   strcap(str);
   puts(str);
   */

    /*
    char a[SIZE] = "foo lfoo";
    censor(a);
    puts(a);
   */

   /* 找最大最小单词 */
   /*
   int n=0;
   char small_word[20],largest_word[20];
   char str[20];
   printf("please Enter:");
   gets(small_word);
   printf("\nplease Enter:");
   gets(largest_word);
   printf("\nplease Enter: ");
   for(;gets(str);){
       if(small_word < str)
            strcpy(small_word,str);
       else if(strcmp(str,largest_word))
            strcpy(largest_word,str);
       if(strlen(str) == 4)
            break;
       printf("\nplease Enter: ");
   }
   printf("\nSmallest world: ");
   puts(small_word);
   printf("\nLargest world: ");
   puts(largest_word);
   */
}

/* 实现read_line函数的几个要求 */
int read_line(char s[]){
    int i=0,j=0;
    char c;
    char countpart[SIZE];
    while((c = getchar()) != ' '){
        if(c != '\n')
            s[i++] = c;
    }
    s[i] = '\n';
    while((c = getchar()) != '\0'){
        countpart[j++] = c;
    }
    countpart[j] = '\0';
    return i;
}

/* 把小写字母转化为大写字母的函数 数组下标版 */
/*
void strcap(char s[]){
    char ch;
    int i,j;
    for(i = 0; i < SIZE ;i++){
        if(s[i] > 'a' && s[i] < 'z'){
           s[i] = toupper(s[i]);
        }
    }
}
*/

/* 把小写字母转化为大写字母的函数 指针版 */
void strcap(char *s){
    char *ch = s;
    int i,j;
    // ch = s; 
    for(;*ch;ch++){
        if(*ch > 'a' && *ch < 'z'){
            *ch = toupper(*ch);       
        }
    }
}

/* 替换特定字符串的函数 */
void censor(char *s){
    char *ch;
    int i,j;
    ch = s;
    for(;*ch;){
        if(*ch == 'f' && *(ch + 1) == 'o' && *(ch + 2) == 'o'){
            *ch = 'x';
            *(ch + 1) = 'x';
            *(ch + 2) = 'x';
            ch = ch + 2;
        }
        ch++;
    }
}


