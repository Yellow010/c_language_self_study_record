/*Author :  Yellow010                  */
/*Propose:  程序结构                    */
/*Date   : 2020/5/23                   */
/*Time   : 22:00-23：30                */

#include<stdio.h>
#include<stdlib.h>

#define NUM_RANKS 13
#define NUM_SUITS 4
#define NUM_CARDS 5
#define STACK_size 10
#define TRUE 1
#define FALSE 0

typedef int BOOL;

void read_cards();
void analyze_hand();
void print_result();

// 外部变量
int num_in_rank[NUM_RANKS];
int num_in_suit[NUM_SUITS];
BOOL straight,flush,four,three;
int pairs;

int content[STACK_size];
int top = 0;

/*int main(){
// 局部变量与外部变量
// 静态变量不会丢失自身的值
    /* 猜数 */
    /*
    int number,rand_num;
    int i=0,j;
    char zi;
    printf("Enter:");
    srand((unsigned) time(NULL));
    for(;;){
        scanf("%d",&number);
        i++;
        rand_num = rand() % 101;
        if(rand_num > number) printf("Too low;Try again");
        else if(rand_num = number){
             printf("You win in %d rounds",i);
             printf("\nAgain?(Y/N)");
             scanf("%c",&zi);
             if(zi == 'Y'){
                 i=0;
                 continue;
             }
             else break;
             
        }
        else printf("To high:Try again");
    }
    */

   /* 构建c语言程序
   1.如#include的预处理指令
   2.类型定义
   3.函数声明与外部变量声明
   4.函数定义
   */

  /* 手牌分类 */
/*    for(;;){
        read_cards();
        analyze_hand();
        print_result();
    }
}

void read_cards(void){
    BOOL card_exists[NUM_RANKS][NUM_SUITS];
    int rank,suit;
    char ch,ch_rank,ch_suit;
    BOOL bad_card;
    int cards_read = 0;

    // 用0填充所有数组
    for(rank = 0;rank < NUM_RANKS;rank++){
        num_in_rank[rank] = 0;
        for(suit = 0;suit < NUM_SUITS;suit++){
            card_exists[rank][suit] = 0;
        }
    }
    for(suit = 0;suit < NUM_SUITS;suit++){
        num_in_suit[suit] = 0;
    }

    printf("Enter: ");
    while(cards_read < NUM_CARDS){
        bad_card = FALSE;

        printf("Enter: ");
        ch_rank = getchar();
        switch(ch_rank){
            case '0': exit(EXIT_SUCCESS);//终止程序
            case '2':rank = 0;break;
            case '3':rank = 1;break;
            case '4':rank = 2;break;
            case '5':rank = 3;break;
            case '6':rank = 4;break;
            case '7':rank = 5;break;
            case '8':rank = 6;break;
            case '9':rank = 7;break;
            case 't':rank = 8;break;
            case 'j':case 'J':rank = 9;break;
            case 'q':case 'Q':rank = 10;break;
            case 'k':case 'K':rank = 11;break;
            case 'a':case 'A':rank = 12;break;
            default: bad_card = TRUE;
        }
        ch_suit = getchar();
        switch (suit)
        {
        case 'c':case 'C':suit = 0;break;
        case 'd':case 'D':suit = 1;break;
        case 'h':case 'H':suit = 2;break;
        case 's':case 'S':suit = 3;break;
        default:bad_card = TRUE;
        }

        while ((ch = getchar()) != '\n')
            if (ch != ' ') bad_card = TRUE;
        if(bad_card) printf("bad card ignore\n");
        else if(card_exists[rank][suit])
            printf("duplicate card ignore\n");
        else{
            num_in_suit[suit]++;
            num_in_rank[rank]++;
            card_exists[rank][suit] = TRUE;
            cards_read++;
        }
    }
}

void analyze_hand(void){
    int num_consec = 0;
    int rank,suit;

    // 类型
    straight = FALSE;
    flush = FALSE;
    three = FALSE;
    four = FALSE;
    pairs = 0;

    for(suit = 0;suit < NUM_SUITS;suit++){
        if(num_in_suit[suit] == NUM_CARDS)
            flush = TRUE;
    }

    rank = 0;
    while(num_in_rank == 0) rank++;
    for(;rank < NUM_RANKS;rank++){
        num_consec++;
    }
    if(num_consec == NUM_CARDS){
        straight = TRUE;
        return;
    }

    for(rank = 0;rank < NUM_RANKS;rank++){
        if(num_in_rank[rank] == 4) four = TRUE;
        if(num_in_rank[rank] == 3) three = TRUE;
        if(num_in_rank[rank] == 2) pairs++;
    }

}

void print_result(void){
    if(straight && flush) printf("Straight flush\n");
    else if(four) printf("Four\n");
    else if(three && pairs ==1) printf("Full House\n");
    else if(flush) printf("Flush\n");
    else if(straight) printf("Straight\n");
    else if(three) printf("Three\n");
    else if(pairs == 2) printf("Two pairs\n");
    else if(pairs == 1) printf("pair\n");
    else printf("High card\n");
}
*/

// 栈的小应用

int main(){
    char ch,temp;
    int n = 0;
    printf("Enter:");
    while((ch = getchar()) != '\n'){
        if(ch == '[' || ch == '{')
            push(ch);
        else if(ch == ']' || ch == '}'){
            temp = pop();
            if(ch == ']' && temp != '[')
                n = 1;
            else if(ch == '}' && temp != '{')
                n = 1;
        }
    }
    if(n == 1)
        printf("False");
    else printf("True");
    return 0;
}

void make_empty(void){
    top = 0;
}

BOOL is_empty(void){
    return top == 0;
}

BOOL is_full(void){
    return top == STACK_size;
}

void push(char i){
    if(!is_full)
        content[top++] = i;
}

int pop(void){
    if(!is_empty)
        return content[--top];
}