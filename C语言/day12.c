/*Author :  Yellow010                  */
/*Propose:  结构，联合与枚举            */

#include<stdio.h>

// 结构最为重要,struct{...}变量;  
// 访问结构是通过名字而不是下标,如 part.number(int) 
// 可直接对part.number进行操作 如：part.number++,part1 = part2
// 结构标记的声明，struct part{...};后面的结构声明就可用 struct part part1,part2来声明
// 结构类型的定义，typdef struct{...}part; 后面就可用 part part1来声明
// 函数也可有结构类型的实际参数与返回值，void Pin(struct part  p)  /   struct part Pin(int ...){struct part p;return p}
// 嵌套结构：结构嵌套结构，数组嵌套结构
// struct{struct ...}访问多打一个点就行，struct part calculate[100]，每一个数组元素都是一套结构，访问calculate[i].number,若number为数组，则可calculate[i].number[i] = 1;
// 结构数组的初始化，struct part code[] = {{"abc" , 12} , {"bcd" , 21} ...}需要用大括号括起来,可作为简单地数据库

// 联合 union{...}u;,联合的成员是存储在同一个地址，访问结构与结构一样。不过值会被覆盖。
// 联合的性质几乎和结构一样
// 使用联合可以节省空间
// 结构与联合可以互相嵌套
// 使用联合来构造不同类型的混合数组，如：typdef union {int number; float length} BOOK;BOOK book1[100] ->这时book1的数组元素可以有不同的类型
// 为联合添加便签来确定确定成员的类型，如：struct{int kind ; union {...}};每当联合成员赋值时，Kind也随之改变

// 枚举是为少量变量设计的一种结构，enum {...} part ;内容默认为整数，顺序为0,1,2,3...
// 创建与声明与结构类似 ，enum{...} part;  enum part part1;typdef enum{...}part;part part1;
// 可为枚举内的成员自由赋值,仅仅表示一种更可能值
// 枚举的值是可以作为整数来使用的，如part1 = 0；part1 ++;
// 为联合做标记 struct{enum{...} kind ; union {...}u}s;

typedef enum{False , True} BOOL;

struct point{
    int x;
    int y;
    // x 与 y轴
    };

struct  rectangle
{
    struct point upper_left,lower_right;
    // 左下与右上点
};


int main(){
    /* 维护零件数据库 */
    /* invent.c */

    /* 第五题：关于矩形的计算 */
    struct rectangle r;
    r.upper_left.x = 10;
    r.upper_left.y = 10;
    r.lower_right.x = 20;
    r.lower_right.y = 20;
    struct point p={0,0};
    p = Center(r);
    printf("center x:%d, center y:%d ",p.x,p.y);
}

// 计算矩形的面积
int S(struct rectangle r){
    float result;
    result = (r.lower_right.y - r.upper_left.y) * (r.lower_right.x - r.upper_left.x);
    return result;
}

// 计算矩形的中心并作为point值返回
struct point Center(struct rectangle r){
    struct point p;
    p.y = (r.lower_right.y - r.upper_left.y) / 2;
    p.x = (r.lower_right.x - r.upper_left.x) / 2;
    return p;
}

// 移动r的位置
struct point Move(struct rectangle r , int x, int y){
    struct point c;
    c = Center(r);
    c.x += x;
    c.y += y;
    return c;
}

// 确定点是否在矩形内
BOOL Judge(struct point p,struct rectangle r){
    if (p.x > r.upper_left.x && p.x < r.lower_right.x){
        if(p.y > r.lower_right.y && p.y < r.upper_left.y){
            return True;
        }
    }
    return False;
}

