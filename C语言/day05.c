/*Author :  Yellow010                  */
/*Propose:  数组                       */
/*Date   : 2020/5/17                   */
/*Time   : 22:00-23：30                */

/* 声明：类型、名字、数量
   如:int a[10]
   为了更好地调整数组长度，可以使用宏定义
   define N 100
   int a[N]
   a[i++] Yes , a[++i] No
*/
#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#define N 10 
#define capital 100
#define SIZE (sizeof(a) / sizeof(a[0]))
int main()
{
    /* 数列反向 */
    /*
    int i = 0,n;
    int a[10];
    printf("请输入10个数字：");
    while(i < N){
        scanf("%d",&n);
        a[i] = n;
        i++;
    }
    for(i = N-1;i >=0;i--){
        printf("%d ",a[i]);
    }
    */

   //数组初始化：int a[10] = {1,2},其余默认为0，但注意{}这样是非法的，要写成{0}
   // 检查数字中是否有重复
   /*
   long int i;
   int a[N] = {0};
   int n;
   printf("Enter:");
   scanf("%ld",&i);
   while(i > 0){
       n = i % 10; 
       if (a[n]) break;
       a[n] = 1;
       i = i/10;
   }
   if (i > 0) printf("Yes");
   else printf("No");
   */
  // 用sizeof来表示数组长度 l=sizeof(a) / sizeof(a[0]),比宏定义更好用
  // define SIZE sizeof(a) / sizeof(a[0]) 需要赋初值

  // 计算利息,谨慎
  /*
  int i,year,rate,j;
  float a[5];
  printf("Enter rate:");
  scanf("%d",&rate);
  printf("Enter years:");
  scanf("%d",&year);
  printf("Years\t");
  for(i = 0;i < 5;i++){
      printf("%d%%\t",rate + i);
      a[i] = capital;
  }
  for(i = 1 ;i <= year ; i++){
      printf("\n%d\t",i);
      for(j = 0;j < 5;j++){
          a[j] = a[j] + a[j] * ((j + rate)/100.0);
          printf("%.2f\t",a[j]);
      }
      printf("\n");
  }
  return 0;
  */

 //多维数组a[i][j],定义时最好用两个大括号，i是行，j是列
 // const 命名数组表示不可修改 如 const int a[10] = {0};

 /* 发牌 */
 //随机抽牌，time函数来自<time.h>,srand函数来自<stdlib.h>,rand函数来自<stdlib.h>
 //避免两次抽到同样的牌
 /*
    int i,j,rand_num;
    int num_cards,grand,color;
    int in_land[4][13] = {0};
    // 不变数组
    const char grands[13] = {'2','3','4','5','6','7','8','9','10','J','Q','K','A'};
    const char colors[4] = {'c','s','d','h'};
    srand((unsigned) time(NULL));  // 随机发牌
    printf("Enter:");
    scanf("%d",&num_cards);
    while(num_cards){
        grand = rand() % 13;  //限制在0-12
        color = rand() % 4;   //限制在0-3
        if(in_land[color][grand] == 0){
            in_land[color][grand] = 1;
            num_cards--;
            printf("  %c%c",grands[grand],colors[color]);
    }
 }
 return 0;
*/

//重点:复制数组，一个是可以用循环，更好的使用memcpy()函数，如将b复制给a memcpy(a,b,sizeof(a))

// 大量的练习
// 01 每种数字出现的次数 #down
   /*
   unsigned long  int num;
   int digit,n,i;
   int O[N] = {0};
   printf("Enter:");
   scanf("%ld",&num);
   while(num > 0){
       n = num % 10;
       O[n] += 1;
       num /= 10; 
   }
   //打印数字
   printf("Digit     ");
   for(i = 0;i < N;i++){
       printf("%d  ",i);
   }
   printf("\nOccurrences  ");
   //打印数组
   for(i = 0;i < N;i++){
       printf("%d  ",O[i]);
   }
   */

// 02 B1FF翻译 用到sizeof(a) / sizeof(a[0])
// 程序是成功了，但是sizeof怎样确定变化的数组大小？
//answer:sizeof(a)/sizeof(a[0]) just for static array；
/*
   char a[10];
   char i,l,j;
   char temp;
   printf("Enter: ");
   // 储存
   for(i=0;i<SIZE;i++){
       scanf("%c",&l);
       a[i] = l;
   }
   // 替换
   for(j = 0;j < SIZE;j++){
       temp = a[j];
       if(temp < 'z' && temp > 'a'){
           temp = temp - 'a' + 'A';
           a[j] = temp;
       }
       switch (temp)
           {
               case 'A':a[j] = '4';break;
               case 'B':a[j] = '8';break;
               case 'E':a[j] = '3';break;
               case 'I':a[j] = '1';break;
               case 'O':a[j] = '0';break;
               case 'S':a[j] = '5';break;
               default:break;
           }
    }
    // 输出
    for(i=0;i<SIZE;i++){
        printf("%c",a[i]);
    }
    printf("!!!!!!!!!!");
*/
// 5*5二维数组求行和与列和
// 正确但觉得有点冗长
/*
   int i,j;
   int a[5][5] = {0};
   int row_sum,clo_sum;
   //输入
   for(i=0;i<5;i++){
       printf("row %d:",i+1);
       scanf("%d %d %d %d %d",&a[i][0],&a[i][1],&a[i][2],&a[i][3],&a[i][4]);
   }
   // 行求和
   printf("Row totles:");
   for(i = 0;i < 5;i++){
       row_sum = 0;
       for(j = 0;j < 5;j++){
           row_sum += a[i][j];
       }
       printf("%d  ",row_sum);
   }
   //列求和
   printf("\ncloumn totles:");
   for(i = 0;i < 5;i++){
       clo_sum = 0;
       for(j = 0;j < 5;j++){ 
           clo_sum += a[j][i];
        }
        printf("%d   ",clo_sum);
    }
*/

// 难点 10*10的‘随机步’
// 爽哉
  const char normal[26] = {'A','B','C','D','E','F','G','H','I','J',
                           'K','L','M','N','O','P','Q','R',
                           'S','T','U','V','W','X','Y','Z'};
  char a[N][N] = {'0'};
  int num,rand_num,n;
  int i,j;
  
  // 输入
  for(i=0;i<N;i++){
      for(j=0;j<N;j++){
          a[i][j] = '.';
      }
  }
  srand((unsigned) time(NULL));
  // 起始点
  a[0][0] = 'A';
  // i为行 ,j为纵
  i=j=0;
  for(n=0;n<26;n++){
      // 随机数
      rand_num = rand() % 4;
      while(rand_num >= 0){
          printf("%d ",rand_num);
          // 判别(数字->是否是点->是否超出范围)
         if(rand_num == 0 && a[i-1][j] == '.' && i-1>=0 ){
             a[i-1][j] = normal[n+1];
             i = i-1;
             break;
         }
         else if(rand_num == 1 && a[i][j+1] == '.' && j+1 < 10){
             a[i][j+1] = normal[n+1];
             j = j+1;
             break;
         }
         else if(rand_num == 2 && a[i+1][j] == '.' && i+1 < 10){
             a[i+1][j] = normal[n+1];
             i = i+1;
             break;
         }
         else if(rand_num == 3 && a[i][j-1] == '.' && j-1 >=0){
             a[i][j-1] = normal[n+1];
             j = j-1;
             break;
         }
         // 都不成立再来
         else rand_num = rand() % 4;
    }
      
  }
  // 输出
  for(i=0;i<N;i++){
      printf("\n");
      for(j=0;j<N;j++){
          printf("   %c",a[i][j]);
      }
  }
  return 0;
}
