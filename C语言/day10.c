/*Author :  Yellow010                  */
/*Propose:  预处理器                   */

#include<stdio.h>
#define a(x) ((x)*(x)*(x))
#define b(x) ((x) / 4)
#define c(x,y) ((x) * (y) < 100? 1 : 0)
#define NELEMS(a) (sizeof(a) / sizeof(a[0]))
#define f(x) ((x) * (x))
#define DISP(f,x) printf("f(%d) = %d\n",x,f(x))
// 预处理器是由指令控制的，而指令一般是由#字开头的
// 宏定义，包含，条件编译，特殊指令
// 如果预处理指令占据多格空间需要用 \ 符号分隔
// 简单的宏可以命名 数字、字符、字符串
// 有趣命名  #define begin {   #define end  }   #define loop for(;;) (循环)
// 类型重命名 #define BOOl int 
// # 用宏定义控制条件编译
// 魔法：带参数的宏 #define MIN(x,y) ((x) < (y)? (x) : (y))   MIN右边没有空格
// #define PRINT(x) printf("%d",x) 大大的好呀
// <ctype.h>中提供了大量的宏定义  
// #字运算符可以将一个宏的参数装转化为 “字符字面量” 如：#define PRINT(x) printf(#x " = %d",x) == printf("x = %d",x);
// ##字符号会将两个符号连在一起 如:#define MARK(x) i##x ; MARK(1) == i1;
// 有时候一个程序写一个max函数时不行的，应为要考虑到不同的类型，所以可以使用宏定义，宏定义有唯一的参数type
/* 
   #define CREAT_MAX (type)
   type type##_MAX(type x,type ,y){
        return x > y? x : y;
   }
*/ //漂亮
// 宏可以包含对另一个宏的使用
// #undef 可以删除宏的定义
// 注意：参数的每一次出现都要添加“圆括号”
// 创建较长的宏时逗号如神来之笔，如:#define ECHO(x)  (gets(x),puts(x))
// 有时我们需要多条语句，安全的办法是使用do..while 语句
// C语言有预定义宏 _LINE_,_FILE_,_DATA_,_TIME_,_STDC_,(_STDC_是判断编译器是否接受标准C，真为1)
// 条件编译NB啊，#if与#endif会判断两个指令之间的类容是否删除 ，如 #define DEBUG 1   #if DEBUG ;puts(str);#endif (如果DEBUG为真则保留)
// defined 函数可以判断是否背定义过宏,通常与#if搭配（没有#号）
// #ifdef 判断是否定义成宏，#ifndef 判断是否没有定义成宏
// #elif 与 #else可以与#if和#endif搭配使用
// #单独一行是空指令
int main(){
   int i,j;
   char str[] = "HAPPY";
   i = 4;
   j = 10;

   printf("%d\n",a(i));
   printf("%d\n",b(i));
   if(c(i,j))
      printf("True");
   else printf("False");
   
  printf("%d",NELEMS(str)); // 结果会多一位因为算上了'\0'
  DISP(f,i);
}
