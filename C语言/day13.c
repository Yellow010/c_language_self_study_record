/*Author :  Yellow010                  */
/*Propose:  指针的高级应用              */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
// 动态存储分配
// malloc()：分配内存块但不初始化,calloc()：分配并且清除内存块置0,realloc()调整先前的内存块
// NULL可用来判断空指针
// 动态分配字符，给n个字符分配空间 p = malloc(n + 1) p的类型为char *
// 给数组动态分配空间 ，如 int *a ; a=malloc(n * sizeof(int)) sizeof很重要
// 使用calloc函数初始化分配空间，默认为0.如 a = calloc(n , sizeof(int))
// 有时候用calloc(1,sizeof(struct p))，1作为实际参数可为任何类型分配空间
// realloc()函数原型 void *realloc(void *ptr,size_t size),后者参数是按要求需要扩大或减少的空间大小
// 当第一个参数为空指针时与malloc()函数一样，当第二个参数为0时，会释放掉内存块
// 重要:释放空间free(void *ptr)
// 释放内存后悔留下 “悬空指针”，要注意
// 链表由节点组成，每个节点包含value和指向下一个节点的指针
// 指向指针的指针见插入函数修改版
// 指向函数的指针，假设有函数 f 作为实际参数用于另一个函数，int calculate(int (*f),int n)
// 在函数f周围添加括号表明是函数，等同于 int calculate(int f , int n)
// 在calculate内部调用 f 写成 sum +=（*f）(x) ; x表示函数f的参数
// qsort()函数，用于对任何数组进行排序，不过需要知道如何比较两个数组元素，且以指针的形式访问数组元素。
// qsort的原函数：void qsort(void *base , size_t nmemb , size_t size , int (*compar)(const void *,const void *))
// base指向数组的第一个元素，nmemb排序数组的数量，size 每个数组元素大小。
// *compar函数需要我们自己写且为void类型，这里以零件存储库为例。
// 指向函数的指针可以用做变量或者数组的元素。
// 如 void (*ptr)(int) 现在ptr可以指向任何类型为int的函数，ptr = f ;  调用函数 (*ptr)(x) / ptr(x) 也就等于f(x)。
// 函数指针的集合非常有用 ，如：void (*ptr[])(void) = {old,new ... },就可以通过ptr[1](x) / (*ptr[1])(x)来调用new指针指向的函数
// qsort中整数排序返回 return *(int *)p - *(int *)q;

struct node{
    int value;
    struct node *next;
};

struct  part
{
    int number;
    char name[10];
    int on_hand;
}inventory[20];

struct {
    char *cmd;
    int (*cmd_pointer)(void);
}file_cmd[] = {{"new",   cmd_new},
               {"old",   cmd_old},
               {"fuck",  cmd_fuck}};

double tab(double (*f)(double) ,double first ,double last,double incr);
int sum(int (*f)(int) , int i, int j);
int twice(int i);

int main(){

    char *p;
    p = concat("asd","dfg");
    puts(*p);

    /* 链表的建立 */
    // 初始化第一个节点
    struct node *first = NULL;
    // 创建节点
    // 分配单元，赋值，插入
    struct node *new;
    new = malloc(sizeof(struct node));

    new->value = 10;// *new.value = 10

    // 插入第一个节点
    new->next = first;
    first = new;

    /* 删除节点 */

    /* 列三角函数表 */
    // cos ,sin ,tan
    /*
    double start , final , increment;
    printf("Enter first number:");
    scanf("%lf",&first);

    printf("Enter final number:");
    scanf("%lf",&final);

    printf("Enter increment number:");
    scanf("%lf",&increment);

    printf("   x      cos(x)   \n");
    printf("------- -------\n");
    tab(cos,start,final,increment);

    printf("   x      cos(x)   \n");
    printf("------- -------\n");
    tab(sin,start,final,increment);

    printf("   x      cos(x)   \n");
    printf("------- -------\n");
    tab(tan,start,final,increment);
    */
   int result;
   result = sum(twice,2,10);
   printf("%d",result);
}

/* 在函数中拼接两个字符串 */
// 不用时free
char *concat(const char *a,const char *b){
    // 先判断两者共需要多少空间，分配再拼接
    char *result;
    result = malloc(strlen(a) + strlen(b) + 1);
    // 判断返回值是否为空
    if( result == NULL){
        printf("It is wrong");
        exit(EXIT_FAILURE);
    }
    strcpy(result,a);
    strcat(result,b);
    return result;
}

/* 插入首节点函数 */
// 这里返回的是指向新节点的指针，first自动更新
struct node *add_list(struct node *start, int n){
    struct node *new;
    new = malloc(sizeof(struct node));

    if(start == NULL){
        printf("It is wrong");
        exit(EXIT_FAILURE);
    }

    new->value = n;
    new->next = start;
    return new;
}

// 修改版
void add_list_two(struct node **start,int n){
    struct node *new;
    new = malloc(sizeof(struct node));

    if(new == NULL){
        printf("It is wrong");
        exit(EXIT_FAILURE);
    }

    new->value =  10;
    new->next = *start;

    *start = new;// 改变了first的指向

}

/* 简单的搜索列表 */
struct node *search_list(struct node *start , int n){
    struct node *p;
    for(p = start ; p != NULL ; p = p->next){
        if(p->value == n){
            return p;
        }
    }
    return NULL;
}

/* 删除节点的函数 */
// 定位，删除，free
// 这里需要添加一个指向前一节点的指针
struct node *delect_list(struct node *start, int n){
    struct node *prev;
    struct node *cur;

    for(cur = start , prev = NULL;cur->value != n && cur != NULL;prev = cur,cur = cur->next);// 定位完成
    if(cur == NULL){
        return start;
    }
    if(prev == NULL){
        start = start->next;
    }// 头结点
    else prev->next = cur->next;

    free(cur);
    return start;// 自动更新
    // *start = cur;
}

/* 比较函数 */
// 这里需要用指针来访问元素及*p与*q
int compar(const struct part *p,const struct part *q){
    struct part *p1 ;
    struct part *q1 ;

    p1 = p;
    q1 = q;

    if(p1->number > q1->number){
        return 1;
    }
    else if(p1->number == q1->number){
        return 0;
    }
    else return -1;
}

/* 列三角函数表 */
double tab(double (*f)(double) ,double first ,double last,double incr){
    double x;
    int num,i;
    num = (last - first) / incr;
    for(i = 0; i < num ;i++){
        x = first + i * incr;
        printf("%12.5f %12.5f\n",x,(*f)(x)) ;
    }
}

/* 课后作业 */

/* malloc包装器 */
void *my_malloc(void *ptr){
    ptr = malloc(sizeof(void));
    if(ptr == NULL){
        printf("It is wrong");
        exit(EXIT_FAILURE);
    }
    return ptr;
}

/* 动态分配并复制字符串 */
/*
char *strdup(char *str){
    char *p;
    //char a[strlen(str) + 1];
    p = malloc(strlen(str) + 1);
    strcpy(p,str);
    return p;
}
*/

/* sum函数 */
int sum(int (*f)(int) , int i, int j){
    int sum=0;
    for(i;i<=j;i++ ){
        sum += (*f)(i);
    }
    return sum;
}

int twice(int i){
    return 2*i;
}


/* 编写函数调用相匹配的命令 */
int *transfer(char *str){
    int i,j;
    int *o;
    for(i = 0;i < 3;i++){
        if(comp(str,file_cmd[i].cmd)){
            return o = file_cmd[i].cmd_pointer;
        }
    }
    return NULL;
}

int cmd_new(int i){
    return i;
}

int cmd_old(int i){
    return -i;
}

int cmd_fuck(int i){
    return 2*i;
}

/* 字符串比较函数 */
int comp(char *str1,char *str2){
    int i,j;
    for(i = 0;i < 3;i++){
        if(str1[i] != str2[i]){
            return -1;
        }    
    }
    return 1;
}

/* 调用函数 */
//void (*file_cmd[])(x)
// (*file_cmd[n])(x)