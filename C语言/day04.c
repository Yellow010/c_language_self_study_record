/*Author :  Yellow010                  */
/*Propose:  基本类型                    */
/*Date   : 2020/5/15                   */
/*Time   : 22:00-23：30                */

/*
 long short (un)signed int/float/double
 u:十进制
 o：八进制
 x:十六进制
 h：短整型
 l:长整形
 char:字符
*/
int main(){
    /* 数列求和改进版，用long*/
    /* 用到%ld格式*/
    /* 对于double类型，只能在scanf中使用%lf，printf中不行*/
    /*
   long int n,sum = 0;
   scanf("%ld", &n);
   while(n != 0)
   {
       sum = sum + n;
       scanf("%ld", &n);
   }
    printf("sum: %ld\n",sum);
    return 0;
    */
    
    /*
       符号转义字符与数字转义字符
       数字转义字符需要用到八进制 如\33 或十六进制 如\x33 来定制 
       一般用宏定义 如 #define ESC '\33'（空格）
    */
    /* 字符处理函数 toupper()把小写装换位大写，需要加上#include<ctype.h>*/
    /* 
       读写字符 ：%c
       如果要跳过读时的空格需要在scanf中加一个空格->scanf(" %c",&c);
       可用getchar 和 putchar 来一个一个的读写字符。
       scanf（）有留下后面字符的习惯,自动跳过空格
    */
    /* 确定消息的长度,包括空格与标点符号，不包过换行符 */
    /* 简直一模一样
    int n = 0;
    char c;
    printf("请输入字符串：");
    scanf("%c",&c);
    while( c != '\n'){
        n += 1;
        scanf("%c",&c);
    }
    printf("长度是：%d",n);
    return 0;
    */
    
    /* sizeof 的运用
       强制转换 printf("%lu",(unsigned long)sizeof(int));
    */
    /* 隐士类型转换（自动）
       强制类型转换（如:(int) float -> 转换成了int）
    */
    /* 类型定义 #define BOOl int == typedef int BOOl;
       移植性更好
    */

   /*practice 01:电话号码转换为数字格式*/
   
   
}