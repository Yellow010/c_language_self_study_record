/* 第12天的维护零件数据库 */
#include<stdio.h>
#include<ctype.h>
//#include"readline.h"

#define NAME_LEN 30
#define MAX_PARTS 100

// 声明结构
struct  part
{
    int number;
    char name[NAME_LEN + 1];
    int on_hand;
}inventory[MAX_PARTS];

// 目前存储的数量
int num_part = 0;

// 声明函数
void Search(void);
void Insert(void);
void Update(void);
void Print(void);
int find_part(int number);
int read_line(char str[],int n);

int main(){
    char code;
    for(;;){
        printf("Please Enter Code:");
        scanf(" %c",&code); // 跳过空格
        switch (code)
    {
        case 'i' : Insert() ;break;
        case 's' : Search() ;break;
        case 'u' : Update() ;break;
        case 'p' : Print() ;break;
        case 'q' : return 0;
        default : printf("U Are Wrong");
    }   
    }
}

int find_part(int number){
    int i;
    for(i = 0;i < num_part ; i++){
        if (inventory[i].number == number){
            return i;
        }
    }
    return -1;
}

void Insert(void){
    int part_number;
    if(num_part == MAX_PARTS){
        printf("Data is full.");
        return;
    }

    printf("PLease Enter Part Number:");
    scanf("%d",&part_number);
    if (find_part(part_number) >= 0){
        printf("Part Exists.");
    }
    else inventory[num_part].number = part_number;

    printf("Please Enter Part Name:");
    read_line(inventory[part_number].name,NAME_LEN);
    //gets(inventory[num_part].name);

    printf("Enter Quantity:");
    scanf("%d",&inventory[part_number].on_hand);
    num_part++;
}

void Search(void){
    int part_number;
    int i;
    printf("Please Enter part number:");
    scanf("%d",&part_number);
    i = find_part(part_number);
    if(i >= 0){
        printf("Part Name:%s\n",inventory[i].name);
        printf("part on_hand；%d\n",inventory[i].on_hand);
    }else printf("Not found");
}

void Update(void){
    int change_number;
    int i;
    int number;
    printf("Please Enter Number:");
    scanf("%d",&number);
    i = find_part(number);
    if(i >= 0){
        printf("Please Enter Change Number:");
        scanf("%d",&change_number);
        inventory[i].on_hand += change_number;
    }
    else printf("NO\n");
}

void Print(void){
    int i;
    printf("Part Number     Part Name    Part on_hand\n");
    for(i = 0;i < num_part;i++){
        printf("%7d    %-30s%11d\n",inventory[i].number,inventory[i].name,inventory[i].on_hand);
    }
}

int read_line(char str[],int n){
    int ch;
    int i = 0;
    while (isspace(ch = getchar()));
    while (ch != '\n' && ch != EOF){
        if (i < n){
            str[i++] = ch;
        }
        ch = getchar();
    }
    str[i] = '\0';
    return i;
}