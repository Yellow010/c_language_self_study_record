/*Author :  Yellow010                  */
/*Propose: 选择语句，循环语句，跳转语句   */
/*Date   : 2020/5/13                   */
/*Time   : 22:00-23：30                */

#include<stdio.h>
int main()
{
/* if else 的运用  */
/* 练习一：计算股票经纪人的佣金 */
/*
    float reward,result;
    printf("请输入交易金额：  ");
    scanf("%f",&reward);
    if(reward <= 2500.00)
        result = reward * 0.017 + 30;
    else if(reward <= 6250.00)
        result = reward * 0.0066 + 56;
    else if(reward <= 20000)
        result = reward * 0.0034 + 76;
    else if(reward <= 50000)
        result = reward * 0.0022 + 100;
    else if(reward <= 500000)
        result = reward * 0.0011 + 155;
    else 
        result = reward * 0.0009 + 255;

    if(result < 39.00)
        result = 39.00;

    printf("您的佣金为：%.2f",result);
    return 0;
    correct 
*/

// 注意else属于最近还未配对的if语句，最好使用大括号来分清楚 
    
/* 简洁表达式
   printf("%d",i > j ? i : j);
*/
    
/* switch 代替级联的if语句 */


/* 练习二：显示法定格式的日期 */
/* 
   enter date(mm/dd/yy) 7/20/93
   dated this 19th day of july,1993
*/

/*
   int day,mon,year;
   printf ("enter date(mm/dd/yy): ");
   scanf ("%d/%d/%d",&mon,&day,&year);

   printf("Dated this %d",day);
   switch(day){
       case 1: case 11:case 21 :
           printf("st");break;
       case 2: case 12: case 22:
           printf("nd");break;
       case 3: case 13: case 23:
           printf("rd"):break;
       default:printf("th");break;
   }
   printf(" day of %d,",mon);
   switch(mon){
       case 1: printf("January"); break;
       case 2: printf("February"); break;
       case 3: printf("March");break;
       case 4: printf("April");break;
       case 5: printf("May");break;
   }
    printf("19%.2d",year);
    */
    /*
    Enter numerical grade : 82
    Letter grade : B
    */
   /*
   int grade;
   int first,second;
   printf("Enter numerical grade: ");
   scanf("%d",&grade);
   if(grade < 0 || grade > 100){
       printf("False");
   }

   first = grade / 10;
   second = grade % 10;

   printf("\nLetter grade ；");
   switch(first){
       case 9:printf("A");break;
       case 8:printf("B");break;
       case 7:printf("C");break;
       case 6:printf("D");break;
       default:printf("F");break;
   }
   */
}
