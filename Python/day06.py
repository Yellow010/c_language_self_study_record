#函数模块 def
#在Python中，函数的参数可以有默认值，也支持使用可变参数，所以Python并不需要像其他语言一样支持函数的重载
#因此在不确定参数个数的时候，我们可以使用可变参数，*args
"""
def add(*args):
    total = 0
    for val in args:
        total += val
    return total

print(add(1))
print(add(1, 2))
"""

#后面定义相同名字的函数会覆盖前面的
#可以使用不同的模块来区分
#from _ import _ as

# __name__是Python中一个隐含的变量它代表了模块的名字
# 只有被Python解释器直接执行的模块的名字才是__main__
# if __name__ == '__main__':

#实现判断一个数是不是回文数的函数
def is_palindrome(num):
    temp = num
    back = 0
    while temp > 0:
        back = back * 10 + temp % 10
        temp //= 10
    return back == num 

# print(is_palindrome(3113))  
 
#在函数中修改全局变量用global x
#事实上，减少对全局变量的使用，也是降低代码之间耦合度的一个重要举措，同时也是对迪米特法则的践行
def main():
    # Todo: Add your code here
    pass


if __name__ == '__main__': #只认自己的窝
    main()