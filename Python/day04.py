#循环结构 for-in while
"""
sum = 0
for i in range(1,101):
    sum += i
print(sum)
"""

"""
#偶数求和
sum = 0
for x in range(2,101,2):
    sum += x
print(sum)
"""

"""
#判断是不是素数
from math import sqrt
x = int(input())
end = int(sqrt(x))
flag = True
for i in range(2,end + 1):
    if x % i == 0:
        flag = False
        break
if flag and x != 1:
    print('yes')
else:
    print('no')
"""

"""
#计算最大公约数与最小公倍数
x = int(input())
y = int(input())
if x > y:
    x,y = y,x
for i in range(x,0,-1):
    if x % i == 0 and y % i == 0:
        print('最大公约数：%d' % (i) )
        print('最小公倍数：%d' % (x * y // i))
        break
"""

#打印图案
row = int(input())
for i in range(row + 1):
    for _ in range(i + 1):
        print('*',end='') 
    print()


for j in range(row + 1):
    for _ in range(row - j):
        print('*',end='')
    print() # 默认换行


