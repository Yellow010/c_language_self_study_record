#语言元素
#应变量名进行运算
a,b = 10,5
c="Hello"
#print(a * b)
#print(a + b)

#用type进行检查数据类型
#print(type(a))
#print(type(c))

"""
转化
int()：将一个数值或字符串转换成整数，可以指定进制。
float()：将一个字符串转换成浮点数。
str()：将指定的对象转换成字符串形式，可以指定编码。
chr()：将整数转换成该编码对应的字符串（一个字符）。
ord()：将字符串（一个字符）转换成对应的编码（整数）
"""
#d = int(input())
#e = int(input())
#print('%d + %d = %d' % (d,e,e+d))

#比较、逻辑运算符
flag0 = 1 == 1
flag1 = 3 < 2
#print('flag1 = ',flag1)

"""
#华氏温度转换为摄氏温度
F = float(input())
H = (F - 32) / 1.8
print(f'摄氏温度为: {H:.1f}摄氏度')
"""

"""
#计算周长与面积
r = int(input())
pi = 3.1415
Area = pi * r * r
Long = 2*pi*r
print(f'圆的面积为：{Area:.2f}圆的周长为：{Long:.2f}')
"""

#判断闰年
year = int(input())
is_leap = year % 4 == 0 and year % 100!= 0 or year % 400 == 0
print(is_leap)
