#面向对象进阶
#包装器包装getter（访问器） and setter（修改器）
#Python内置的@property装饰器就是负责把一个方法变成属性调用
class Person(object):
    #对绑定新属性进行限制
    __slots__ = ('_name','_age','_gender')

    def __init__(self,name,age,gender):
        self._name = name
        self._age = age
        self._gender = gender

    #访问器gatter
    @property
    def name(self):
        return self._name
    #这里只读

    @property
    def age(self):
        return self._age

    #修改器setter
    #当成了属性来用就不用返回
    @age.setter
    def age(self,age):
        self._age = age
    #这里可读可写
    
    def play(self):
        if self._age <= 6:
            print("%s正在哈哈哈" % self._name)
        else:
            print("%s正在打游戏" % self._name)

def main():
    #创建对象
    person = Person('张三',18,'男')
    person.play()
    person.age = 5
    person.play()

'''
if __name__=='__main__':
    main()
'''

#但是如果我们需要限定自定义类型的对象只能绑定某些属性，
# 可以通过在类中定义__slots__变量来进行限定。
# 需要注意的是__slots__的限定只对当前类的对象生效，对子类并不起任何作用。
#用法如 __slots__ = ('_name','_age','_gender')

#当我们在类中定义不是传递信息给对象的方法就可以使用静态方法
#例如判断三角形类中三条边能否构成三角形
from math import sqrt

class Triangle(object):
    __slot__ = ('_a','_b','_c')

    def __init__(self,a,b,c):
        self._a = a
        self._b = b
        self._c = c

    @staticmethod
    #没有对象参数
    def is_valid(a,b,c):
        return a+b>c and a+c>b and b+c>a

    #计算周长与面积
    def perimeter(self):
        return self._a + self._b + self._c

    def area(self):
        half = self.perimeter() / 2
        return sqrt(half * (half - self._a) *\
                    (half - self._b) * (half - self._c))

def main1():
    a,b,c = 2,3,4
    if Triangle.is_valid(a,b,c):
        triangle = Triangle(a,b,c)
        print(triangle.perimeter())
        print(triangle.area())
    else:
        print("fuck off")

'''
if __name__ == '__main__':
    main1()
'''

#定义类方法，用关键字cls，它代表的是当前类相关的信息的对象
#通过这个参数我们可以获取和类相关的信息并且可以创建出类的对象
#比如就可以获得类中的私有属性

from time import time, localtime, sleep

'''
class Clock(object):
    """数字时钟"""

    def __init__(self, hour=0, minute=0, second=0):
        self.__hour = hour
        self.__minute = minute
        self.__second = second

    @classmethod
    def now(cls):
        ctime = localtime(time())
        return cls(ctime.tm_hour, ctime.tm_min, ctime.tm_sec)

    def run(self):
        """走字"""
        self.__second += 1
        if self.__second == 60:
            self.__second = 0
            self.__minute += 1
            if self.__minute == 60:
                self.__minute = 0
                self.__hour += 1
                if self.__hour == 24:
                    self.__hour = 0

    def show(self):
        """显示时间"""
        return '%02d:%02d:%02d' % \
               (self.__hour, self.__minute, self._second)


def main():
    # 通过类方法创建对象并获取系统时间
    clock = Clock.now()
    while True:
        print(clock.show())
        sleep(1)
        clock.run()


if __name__ == '__main__':
    main()
'''

#类之间的关系：继承，关联与依赖
#UML面向对象建模语言《UML面向对象设计基础》

#继承与多态
#继承是自雷从父类那里继承它的信息与方法，并且定义自己的出行与方法
'''
class Person(object):
    def __init__(self,name,age):
        self._name = name
        self._age = age

    @property
    def name(self):
        return self._name

    @property
    def age(self):
        return self._age

    @age.setter
    def age(self,age):
        self._age = age
    
    def play(self):
        if self._age <= 6:
            print("%s正在哈哈哈" % self._name)
        else:
            print("%s正在打游戏" % self._name)

class Student(Person):
    #父类也是超类
    def __init__(self,name,age,grade):
        super().__init__(name,age)
        self._grade = grade

    @property
    def grade(self):
        return self._grade

    @grade.setter
    def grade(self,age):
        self._age = age

    def study(self):
        print("一个%s年级叫%s的学生正在打电视" % (self._grade,self._name))    

class Teacher(Person):
    def __init__(self,name,age,title):
        super().__init__(name,age)
        self._title = title

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self, title):
        self._title = title

    def teach(self, course):
        print('%s%s正在讲%s.' % (self._name, self._title, course))

def main():
    #创建对象
    stu = Student('张三',10,2)
    stu.study()
    stu.age = 20
    #print(stu._age)都可以
    #偏向于下面这种
    print(stu.age)

    tea = Teacher('石头',78,'菜鸟')
    tea.title = '大师'
    tea.teach('恋爱指南')

'''

#if __name__ == '__main__':
#    main()


#重写（override）即重写父类的方法
#重写的五花八门即多态
#必须的
#抽象类只为继承所用
'''
from abc import ABCMeta, abstractmethod
class Pet(object, metaclass=ABCMeta):
    """宠物"""
    #抽象类，不能够创建对象的类

    def __init__(self, nickname):
        self._nickname = nickname

    @abstractmethod
    def make_voice(self):
        """发出声音"""
        pass


class Dog(Pet):
    """狗"""
    def make_voice(self):
        print('%s: 汪汪汪...' % self._nickname)

class Cat(Pet):
    def make_voice(self):
        print("%s:喵喵喵" % self._nickname)

def main():
    pets = [Cat('小明'),Dog('未来')]
    for pet in pets:
        pet.make_voice()

if __name__ == '__main__':
    main()

'''

'''
from abc import ABCMeta, abstractmethod
from random import randint, randrange


class Fighter(object, metaclass=ABCMeta):
    """战斗者"""

    # 通过__slots__魔法限定对象可以绑定的成员变量
    __slots__ = ('_name', '_hp')

    def __init__(self, name, hp):
        """初始化方法

        :param name: 名字
        :param hp: 生命值
        """
        self._name = name
        self._hp = hp

    @property
    def name(self):
        return self._name

    @property
    def hp(self):
        return self._hp

    @hp.setter
    def hp(self, hp):
        self._hp = hp if hp >= 0 else 0

    @property
    def alive(self):
        return self._hp > 0

    @abstractmethod
    def attack(self, other):
        """攻击

        :param other: 被攻击的对象
        """
        pass


class Ultraman(Fighter):
    """奥特曼"""

    __slots__ = ('_name', '_hp', '_mp')

    def __init__(self, name, hp, mp):
        """初始化方法

        :param name: 名字
        :param hp: 生命值
        :param mp: 魔法值
        """
        super().__init__(name, hp)
        self._mp = mp

    def attack(self, other):
        other.hp -= randint(15, 25)

    def huge_attack(self, other):
        """究极必杀技(打掉对方至少50点或四分之三的血)

        :param other: 被攻击的对象

        :return: 使用成功返回True否则返回False
        """
        if self._mp >= 50:
            self._mp -= 50
            injury = other.hp * 3 // 4
            injury = injury if injury >= 50 else 50
            other.hp -= injury
            return True
        else:
            self.attack(other)
            return False

    def magic_attack(self, others):
        """魔法攻击

        :param others: 被攻击的群体

        :return: 使用魔法成功返回True否则返回False
        """
        if self._mp >= 20:
            self._mp -= 20
            for temp in others:
                if temp.alive:
                    temp.hp -= randint(10, 15)
            return True
        else:
            return False

    def resume(self):
        """恢复魔法值"""
        incr_point = randint(1, 10)
        self._mp += incr_point
        return incr_point

    def __str__(self):
        return '~~~%s奥特曼~~~\n' % self._name + \
            '生命值: %d\n' % self._hp + \
            '魔法值: %d\n' % self._mp


class Monster(Fighter):
    """小怪兽"""

    __slots__ = ('_name', '_hp')

    def attack(self, other):
        other.hp -= randint(10, 20)

    def __str__(self):
        return '~~~%s小怪兽~~~\n' % self._name + \
            '生命值: %d\n' % self._hp


def is_any_alive(monsters):
    """判断有没有小怪兽是活着的"""
    for monster in monsters:
        if monster.alive > 0:
            return True
    return False


def select_alive_one(monsters):
    """选中一只活着的小怪兽"""
    monsters_len = len(monsters)
    while True:
        index = randrange(monsters_len)
        monster = monsters[index]
        if monster.alive > 0:
            return monster


def display_info(ultraman, monsters):
    """显示奥特曼和小怪兽的信息"""
    print(ultraman)
    for monster in monsters:
        print(monster, end='')


def main():
    u = Ultraman('骆昊', 1000, 120)
    m1 = Monster('狄仁杰', 250)
    m2 = Monster('白元芳', 500)
    m3 = Monster('王大锤', 750)
    ms = [m1, m2, m3]
    fight_round = 1
    while u.alive and is_any_alive(ms):
        print('========第%02d回合========' % fight_round)
        m = select_alive_one(ms)  # 选中一只小怪兽
        skill = randint(1, 10)   # 通过随机数选择使用哪种技能
        if skill <= 6:  # 60%的概率使用普通攻击
            print('%s使用普通攻击打了%s.' % (u.name, m.name))
            u.attack(m)
            print('%s的魔法值恢复了%d点.' % (u.name, u.resume()))
        elif skill <= 9:  # 30%的概率使用魔法攻击(可能因魔法值不足而失败)
            if u.magic_attack(ms):
                print('%s使用了魔法攻击.' % u.name)
            else:
                print('%s使用魔法失败.' % u.name)
        else:  # 10%的概率使用究极必杀技(如果魔法值不足则使用普通攻击)
            if u.huge_attack(m):
                print('%s使用究极必杀技虐了%s.' % (u.name, m.name))
            else:
                print('%s使用普通攻击打了%s.' % (u.name, m.name))
                print('%s的魔法值恢复了%d点.' % (u.name, u.resume()))
        if m.alive > 0:  # 如果选中的小怪兽没有死就回击奥特曼
            print('%s回击了%s.' % (m.name, u.name))
            m.attack(u)
        display_info(u, ms)  # 每个回合结束后显示奥特曼和小怪兽的信息
        fight_round += 1
    print('\n========战斗结束!========\n')
    if u.alive > 0:
        print('%s奥特曼胜利!' % u.name)
    else:
        print('小怪兽胜利!')


if __name__ == '__main__':
    main()
'''

#工资结算系统
'''
计算月薪
部门经理每月15000元
程序员每月按小时计算，每小时150元
销售员1200元每月底薪+销售额5%提成
'''

from abc import ABCMeta,abstractmethod

class Employee(object,metaclass=ABCMeta):
    '''
    雇员
    属性：名字、职称
    无法单纯的用Manager创建对象了
    '''
    __slots__=('_name','_title')

    def __init__(self,name,title):
        self._name = name
        self._title = title

    #名字与职称都不可修改
    @property
    def name(self):
        return self._name

    @property
    def title(self):
        return self._title

    @abstractmethod
    def money_of_month(self):
        pass

class Manager(Employee):
    '''
    经理
    '''
    def __init__(self,name,title):
        super().__init__(name,title)

    def money_of_month(self):
        return 15000

class Programmer(Employee):
    '''程序员'''
    def __init__(self,name,title):
        super().__init__(name,title)
    
    def money_of_month(self,hours):
        return 150*hours

class Salesman(Employee):
    '''销售员'''
    def __init__(self,name,title):
        super().__init__(name,title)

    def money_of_month(self,total):
        return 1200 + total*0.05


def main2():
    #创建三个对象
    manager = Manager('王鲲','经理')
    programmer = Programmer('老马','程序员')
    salesman = Salesman('小马','销售员')
    print("%s%s的工资是%d:"%(manager.name,manager.title,manager.money_of_month()))
    print("%s%s的工资是%d:"%(programmer.name,programmer.title,programmer.money_of_month(100)))
    print("%s%s的工资是%d"%(salesman.name,salesman.title,salesman.money_of_month(10000)))

if __name__=='__main__':
    main2()