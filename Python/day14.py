#进程与线程
#Python的os模块提供了fork()函数
#使用multiprocessing模块的Process类来创建子进程

from multiprocessing import Process
from os import getpid
from random import randint
from time import time,sleep
from threading import Thread,Lock

#简单多进程演示
#通过target参数我们传入一个函数来表示进程启动后要执行的代码，
#后面的args是一个元组，它代表了传递给函数的参数。
#join方法表示等待进程执行结束
#getpid获得子进程的id

'''
def task(file):
    print('启动下载进程，进程号[%d]' % getpid())
    print('开始下载%s' % file)
    time_task = randint(5,10)
    sleep(time_task)
    print('%s任务完成用了%d秒'%(file,time_task))

def main():
    #创建类的对象（子进程）
    start = time()
    p1 = Process(target=task,args=('Python宇宙最强',))
    p1.start()
    p2 = Process(target=task,args=('c++宇宙最强',))
    p2.start()
    p1.join()
    p2.join()
    end = time()
    print('总共花费了%.2f时间'%(start - end))
'''

#使用subprocess模块中的类和函数来创建和启动子进程，然后通过管道来和子进程通信
#子进程复制了父进程及其所有的数据结构，
#每个子进程有自己独立的内存空间
#要解决这个问题比较简单的办法是使用multiprocessing模块中的Queue类，
#它是可以被多个进程共享的队列，


#多线程的问题
#Python中使用线程有两种方式：函数或者用类来包装线程对象
'''
def task(file):
    print('启动下载进程，进程号[%d]' % getpid())
    print('开始下载%s' % file)
    time_task = randint(5,10)
    sleep(time_task)
    print('%s任务完成用了%d秒'%(file,time_task))

def main():
    #创建类的对象（子进程）
    start = time()
    p1 = Thread(target=task,args=('Python宇宙最强',))
    p1.start()
    p2 = Thread(target=task,args=('c++宇宙最强',))
    p2.start()
    p1.join()
    p2.join()
    end = time()
    print('总共花费了%.2f时间'%(start - end))
'''

#继承多线程的类，自定义
#这个有问题，是按顺序运行的（GIL）
#run是Thread就包含的函数
'''
class Task(Thread):
    def __init__(self,filename):
        super().__init__()
        self._filename = filename

    def run(self):
        print('开始下载%s' % self._filename)
        time_task = randint(5,10)
        sleep(time_task)
        print('%s任务完成用了%d秒'%(self._filename,time_task))

def main():
    start = time()
    t1 = Task('Python宇宙最强')
    t1.start()
    t2 = Task('c++宇宙最强')
    t2.start()
    t1.join()
    t2.join()
    end = time()
    print("项目总共花费了%.2f" % (start - end))
'''


#“临界资源”的访问需要加上保护，否则资源会处于“混乱”的状态
#需要用锁来临时锁住一个线程
#GIL 限制了同一时刻只能有一个线程运行，无法发挥多核 CPU 的优势

#100个线程存入银行账户1元

'''
class Account_Bank(object):
    def __init__(self,balance = 0):
        self._balance = balance
        self._lock = Lock()

    def addmoney(self,money):
        #先获得锁后才可以向下执行
        self._lock.acquire()
        try:
            self._balance += money
        finally:
            #在finally中执行释放锁的操作保证正常异常锁都能释放
            self._lock.release()

    @property
    def bankmoney(self):
        return self._balance

class  AddThread(Thread):
    
    def __init__(self,account,money):
        super().__init__()
        self._account = account
        self._money = money

    #一个线程的工作主要是加钱
    def run(self):
        self._account.addmoney(self._money)


def main():
    #创建100个线程
    account = Account_Bank()
    Threadlist = []
    for _ in range(100):
        t = AddThread(account,1)
        t.start()
        Threadlist.append(t)

    for t in Threadlist:
        t.join()
    
    print('总资产为：%d' % account._balance)
'''

#如果充分利用操作系统提供的异步I/O支持，就可以用单进程单线程模型来执行多任务，
#这种全新的模型称为事件驱动模型
#单线程+异步I/O的编程模型称为协程
#如果想要充分利用CPU的多核特性，最简单的方法是多进程+协程

#可以将耗时间的任务单独放到一个线程中运行
#使用多进程将问题分别处理

#我们来完成1~100000000求和的计算密集型任务
#分到8进程中完成

def cal_num(start,end):
    sum = 0
    for i in range(start,end+1):
        sum += i
    return sum

'''
#创建8个进程
def main():
    sub = 100000000
    process_num = 8
    processlist = []
    min = sub // 8
    start = time()
    for i in range(process_num):
        t = Process(target=cal_num,args=(min*i,min*(i+1)))
        processlist.append(t)
        t.start()
    
    for t in processlist:
        t.join()
    end = time()
    print('总共花费时间为：%.2fs' % (start - end))
#如果要得到结果加一个队列或栈就行了
'''

#可以将多个进程部署在不同的计算机上，做成分布式进程，
#具体的做法就是通过multiprocessing.managers模块中提供的管理器将Queue对象通过网络共享出来



if __name__ == '__main__':
    main()
        
