#面向对象基础
#把一组数据结构和处理它们的方法组成对象（object），
# 把相同行为的对象归纳为类（class），通过类的封装（encapsulation）隐藏内部细节，
# 通过继承（inheritance）实现类的特化（specialization）和泛化（generalization），
# 通过多态（polymorphism）实现基于对象类型的动态分派。

#定义类
'''
class student(object):
    #_init_初始化操作
    #定义name,age两个属性
    def __init__(self,name,age):
        self.name = name
        self.age = age
    
    #对象的方法
    def play(self,room):
        print('%s正在%d号房间玩游戏'%(self.name,room))
'''

'''
#创建对象，给对象发消息
def main():
    stu = student('鲲',18)
    stu.play(2)

if __name__ == "__main__":
    main()
'''

#如果你希望属性或者方法是私有的可以在前面加两个下划线
#但是保护的并不严格
'''
def _init_(self,xoxo):
    self.__xoxo =xoxo

def __act(self):
    print('__act')
'''

#在实际开发中，我们并不建议将属性设置为私有的，因为这会导致子类无法访问

#封装：隐藏一切实现细节只提供接口
#只需要知道方法名字与参数

#定义一个类描述数字时钟。
from time import sleep
'''
class Timeclock(object):
    
    #显示时与分，也就是时钟的两个属性
    
    def __init__(self,hour,minute,second):
        self._hour = hour
        self._minute = minute
        self._second = second
        self._max_count = 60 #相当于进位区间
    
    def start(self):
        #开始走
        self._second+=1
        if self._second > self._max_count:
            self._minute += 1
            self._second = 0
            if self._minute > self._max_count:
                self._hour+=1
                self._minute=0
                if self._hour > 24:
                    self._hour = 0
    
    def show(self):
        #显示
        return '%02d%02d%02d' % (self._hour,self._minute,self._second)

def main():
    #创建对象
    clock = Timeclock(1,1,2)
    #开始走
    while True:
        clock.start()
        sleep(3)
        print(clock.show())
'''

#定义一个类描述平面上的点并提供移动点和计算到另一个点距离的方法
from math import sqrt
#sqrt 开方
class point(object):
    '''
    规定平面大小60*60
    点的横纵坐标
    col 横坐标
    ind 纵坐标
    '''
    def __init__(self,col,ind):
        self._col = col
        self._ind = ind
    
    def creatify(self):
        if self._col > 60:
            self._col = 60
        if self._ind > 60:
            self._ind = 60

def calremote(point1,point2):
    dx = point1._col - point2._col
    dy = point1._ind - point2._ind
    S = dx**2 + dy**2
    return sqrt(S)

def main():
    point1 = point(22,12)
    point2 = point(76,5)
    point1.creatify()
    point2.creatify()
    #print(calremote(point1,point2))
    return calremote(point1,point2)

if __name__ == "__main__":
    main()




        

