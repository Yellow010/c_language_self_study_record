#字符串和正则表达式
#python通过标准库中的re模块来支持正则表达式操作
#https://deerchao.cn/tutorials/regex/regex.htm
#!
#挺好玩的

#主要函数
'''
compile(pattern, flags=0)	编译正则表达式返回正则表达式对象
match(pattern, string, flags=0)	用正则表达式匹配字符串 成功返回匹配对象 否则返回None
search(pattern, string, flags=0)	搜索字符串中第一次出现正则表达式的模式 成功返回匹配对象 否则返回None
split(pattern, string, maxsplit=0, flags=0)	用正则表达式指定的模式分隔符拆分字符串 返回列表
sub(pattern, repl, string, count=0, flags=0)	用指定的字符串替换原字符串中与正则表达式匹配的模式 可以用count指定替换的次数
fullmatch(pattern, string, flags=0)	match函数的完全匹配（从字符串开头到结尾）版本
findall(pattern, string, flags=0)	查找字符串所有与正则表达式匹配的模式 返回字符串的列表
finditer(pattern, string, flags=0)	查找字符串所有与正则表达式匹配的模式 返回一个迭代器
'''

import re
"""
验证输入用户名和QQ号是否有效并给出对应的提示信息

要求：用户名必须由字母、数字或下划线构成且长度在6~20个字符之间，QQ号是5~12的数字且首位不能为0
"""


'''
#注意这里的r表示原始字符串，就是里面没有转义字符
def main():
    username = input('请输入用户名：')
    qq = input('请输入QQ号：')

    #输入字符串比较字符串
    u1 = re.match(r'^\w{6,20}$',username)
    if not u1:
        print('用户名信息有误！')

    q1 = re.match(r'^[1-9]\d{4,11}$',qq)
    if not q1:
        print('输入QQ有误')

'''

'''
#匹配运营商的手机号码
def main():
    #正则表达式的对象
    #开头结尾不是数字，匹配中间的11位数字
    pattern = re.compile(r'(?<=\D)1[34578]\d{9}(?=\D)')

    #打开文件
    try:
        with open('D:\\Python_files\\Python_100days\\text.txt','r',encoding='utf-8') as file:
            sentence = file.read()    
            print(sentence)
    except FileNotFoundError:
        print('没找到')
    except LookupError:
        print('码没了')
    except UnicodeDecodeError:
        print('码错了')

    #三种方法取出
    #在句子中匹配模式
    mylist = re.findall(pattern,sentence)
    print(mylist)

    print('-----------------')
    #通过迭代器取出匹配对象
    for temp in pattern.finditer(sentence):
        print(temp.group())#不分组全取出来

    print('-----------------')
    #自动搜索位置找出所有匹配

    pos = pattern.search(sentence)
    while pos:
        print(pos.group())
        pos = pattern.search(sentence,pos.end())
'''

'''
#替换文字中的不良内容
def main():
    sentence = '你丫是傻叉吗? 我操你大爷的. Fuck you.'
    repl = re.sub(r'[操肏艹]|fuck|shit|傻[比屄逼叉缺吊屌]|煞笔','*',sentence,flags = re.IGNORECASE)
    print(repl)
'''

#拆分长字符串
def main():
    poem = '窗前明月光，疑是地上霜。举头望明月，低头思故乡。'
    sentence = re.split(r'[,.，。]',poem)
    print(sentence)
    while '' in sentence:
        sentence.remove('')
    print(sentence[2])

if __name__=='__main__':
    main()

