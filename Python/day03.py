#分支结构

"""
#分段函数求值
x = float(input())
if x > 100:
    y = 3*x - 5
elif x < 1 and x >-1:
    y = x + 2
else:
    y = 2*x
print('f(%.2f) = %.2f' % (x,y))
"""

"""
#英寸与厘米互换
#先判断是英寸还是厘米
s = int(input('英寸转厘米请输入1，反之请输入2：'))
value = float(input('请输入长度：'))
if s==1:
    print('%f厘米：'%(value / 2.54))
elif s==2:
    print('%f英寸：'%(value * 2.54))
"""

#百分制转等级制

#输入三条边长，若能构成三角形就计算面积
a = float(input('a='))
b = float(input('b='))
c = float(input('c='))
if a+b > c and a+c >b and b+c > a:
    p = (a + b +c)/2
    area = (p * (p-a) * (p-b) * (p-c)) **0.5
    print('面积为：%.2f'%(area))
else:
    print('回家吃饭吧')
