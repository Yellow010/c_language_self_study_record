'''
面向对象设计原则

单一职责原则 （SRP）- 一个类只做该做的事情（类的设计要高内聚）
开闭原则 （OCP）- 软件实体应该对扩展开发对修改关闭
依赖倒转原则（DIP）- 面向抽象编程（在弱类型语言中已经被弱化）
里氏替换原则（LSP） - 任何时候可以用子类对象替换掉父类对象
接口隔离原则（ISP）- 接口要小而专不要大而全（Python中没有接口的概念）
合成聚合复用原则（CARP） - 优先使用强关联关系而不是继承关系复用代码
最少知识原则（迪米特法则，LoD）- 不要给没有必然联系的对象发消息
'''

'''
GoF设计模式

创建型模式：单例、工厂、建造者、原型
结构型模式：适配器、门面（外观）、代理
行为型模式：迭代器、观察者、状态、策略
'''


'''
迭代器与生成器
__iter__和__next__魔术方法就是迭代器协议。

'''

#用迭代器协议实现迭代器

class Fib(object):
    def __init__(self,num):
        self.num = num
        self.a,self,b = 0,1
        self.count = 0

    def __iter__(self):
        return self


    def __next__(self):
        if self.idx < self.num:
            self.a, self.b = self.b, self.a + self.b
            self.idx += 1
            return self.a

#生成器是语法简化版的迭代器。
def fib(num):
    """生成器"""
    a, b = 0, 1
    for _ in range(num):
        a, b = b, a + b
        yield a

#print(fib(2))

'''
生成器进化为协程。
生成器对象可以使用send()方法发送数据，发送的数据会成为生成器函数中通过yield表达式获得的值。
这样，生成器就可以作为协程使用，协程简单的说就是可以相互协作的子程序。
'''

def calc():
    #计算平均值
    total,counter = 0,0
    avg = None
    while True:
        #捕获一次
        value = yield avg
        total = total + value
        counter += 1
        avg = total / counter

gen = calc()
next(gen)
#print(gen.send(10))
#print(gen.send(20))
#print(gen.send(30))

'''
并发编程
Python中实现并发编程的三种方案：多线程、多进程和异步I/O
多线程：Python中提供了Thread类并辅以Lock、Condition、Event、Semaphore和Barrier。
       Python中有GIL来防止多个线程同时执行本地字节码，这个锁对于CPython是必须的，
       因为CPython的内存管理并不是线程安全的，因为GIL的存在多线程并不能发挥CPU的多核特性。
多线程程序如果没有竞争资源处理起来通常也比较简单
当多个线程竞争临界资源的时候如果缺乏必要的保护措施就会导致数据错乱

'''

#实例，多个用户向银行指定账号存钱

import time
import threading
from random import randint
#线程池
#用完的线程会返回等待不会倍销毁
from concurrent.futures import ThreadPoolExecutor

class Account(object):
    def __init__(self):
        self.balance = 0.0
        self.lock = threading.Lock()

    def deposit(self,money):
        #变化后的余额
        new_balance = money+self.balance
        time.sleep(0.01)
        self.balance = new_balance

#自定义线程
class AddMoneyThread(threading.Thread):
    def __init__(self,account,money):
        self.account = account
        self.money = money
        # 自定义线程的初始化方法中必须调用父类的初始化方法
        super().__init__()

    def run(self):
        # 线程启动之后要执行的操作
        self.account.deposit(self.money)

def main():
    #初始化对象
    account = Account()   

    # 创建线程池
    pool = ThreadPoolExecutor(max_workers=10)

    futures = []
    for _ in range(30):
        # 创建线程的第1种方式
        # threading.Thread(
        #     target=account.deposit, args=(1, )
        # ).start()
        # 创建线程的第2种方式
        # AddMoneyThread(account, 1).start()
        # 创建线程的第3种方式
        # 调用线程池中的线程来执行特定的任务
        future = pool.submit(account.deposit, 1)
        futures.append(future)

    #关闭线程池
    pool.shutdown()

    for future in futures:
        future.result()

    #print(account.balance)


#修改上面的程序，启动5个线程向账户中存钱，5个线程从账户中取钱，取钱时如果余额不足就暂停线程进行等待
'''
多个线程竞争一个资源 - 保护临界资源 - 锁（Lock/RLock）
多个线程竞争多个资源（线程数>资源数） - 信号量（Semaphore）
多个线程的调度 - 暂停线程执行/唤醒等待中的线程 - Condition
'''

class NewAccount(object):
    def __init__(self):
        self.balance = 0.0
        self.lock = threading.Lock()
        #每次都与一个锁相关联
        self.condition = threading.Condition(self.lock)

    def deposit(self,money):
        #存钱
        with self.condition:
            new_balance = money+self.balance
            time.sleep(0.01)
            self.balance = new_balance
            #通知
            self.condition.notify_all()

    def withdrow(self,money):
        #取钱
        with self.condition:
            while money > self.balance:
                self.condition.wait()
            new_balance = self.balance - money
            time.sleep(0.01)
            self.balance = new_balance

def add_money(account):
    while True:
        money = randint(5, 10)
        account.deposit(money)
        print(threading.current_thread().name, 
              ':', money, '====>', account.balance)
        time.sleep(1)

def sub_money(account):
    while True:
        money = randint(10, 30)
        account.withdraw(money)
        print(threading.current_thread().name, 
              ':', money, '<====', account.balance)
        time.sleep(1)

def main2():
    #创建线程池
    account = NewAccount()
    with ThreadPoolExecutor(max_workers = 15) as pool:
        for _ in range(10):
            pool.submit(add_money,account)
        for _ in range(5):
            pool.submit(sub_money,account)
    print(account.balance)

'''
多进程：多进程可以有效的解决GIL的问题，实现多进程主要的类是Process，其他辅助的类跟threading模块中的类似，
进程间共享数据可以使用管道、套接字等，在multiprocessing模块中有一个Queue类，它基于管道和锁机制提供了多个进程共享的队列。
'''

'''
重点：多线程和多进程的比较。

以下情况需要使用多线程：

程序需要维护许多共享的状态（尤其是可变状态），Python中的列表、字典、集合都是线程安全的，所以使用线程而不是进程维护共享状态的代价相对较小。
程序会花费大量时间在I/O操作上，没有太多并行计算的需求且不需占用太多的内存。

以下情况需要使用多进程：

程序执行计算密集型任务（如：字节码操作、数据处理、科学计算）。
程序的输入可以并行的分成块，并且可以将运算结果合并。
程序在内存使用方面没有任何限制且不强依赖于I/O操作（如：读写文件、套接字等）
'''

'''
异步处理
异步任务通常通过多任务协作处理的方式来实现，由于执行时间和顺序的不确定，因此需要通过回调式编程或者future对象来获取任务执行的结果
Python 3通过asyncio模块和await和async关键字

要实现任务的异步化，可以使用名为Celery的三方库。Celery是Python编写的分布式任务队列，它使用分布式消息进行工作，可以基于RabbitMQ或Redis来作为后端的消息代理。
'''