#构造程序逻辑

"""
#水仙花数
#找出100-1000的水仙花数
for x in range(100,1001):
    a = x // 10 % 10 #2
    b = x % 10 #3
    c = x // 100 # 1
    if a*a*a + b*b*b + c*c*c == x:
        print(x)
"""

"""
#百钱百鸡
for x in range(20):
    for y in range(33):
        z = 100 - x - y
        if 5*x + 3*y + z/3 == 100:
            print('%d只公鸡，%d母鸡，%d小鸡' % (x,y,z))
"""

#斐波那契数列
a,b = 1,1
print(a)
for _ in range(1,20):
    print(b)
    a,b = b,a+b
