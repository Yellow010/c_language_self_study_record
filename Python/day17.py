#进阶

#生成式的用法，用于生成字典列表或者集合
prices = {
    'AAPL': 191.88,
    'GOOG': 1186.96,
    'IBM': 149.24,
    'ORCL': 48.44,
    'ACN': 166.89,
    'FB': 208.09,
    'SYMC': 21.29
}
#把股票价格大于100的提取出来
prices2 = {key:value for key,value in prices.items() if value > 100}
prices3 = [value for value in prices.values() if value > 60]
#print(prices2)
#print(prices3)

#笛卡尔积
'''
names = ['关羽', '张飞', '赵云', '马超', '黄忠']
courses = ['语文', '数学', '英语']
# 录入五个学生三门课程的成绩
# 错误 - 参考http://pythontutor.com/visualize.html#mode=edit
# scores = [[None] * len(courses)] * len(names)
scores = [[None] * len(courses) for _ in range(len(names))]
for row, name in enumerate(names):
    for col, course in enumerate(courses):
        scores[row][col] = float(input(f'请输入{name}的{course}成绩: '))
        #print(scores)
'''

#heapq模块（堆排序）
"""
从列表中找出最大的或最小的N个元素
堆结构(大根堆/小根堆)
"""

#匿名函数lambda：是指一类无需定义标识符（函数名）的函数或子程序。
#lambda 函数可以接收任意多个参数 (包括可选参数) 并且返回单个表达式的值。

import heapq

list1 = [12,45,7,896,45,61,3,15,5555,0]
list2 = [
    {'name': 'IBM', 'shares': 100, 'price': 91.1},
    {'name': 'AAPL', 'shares': 50, 'price': 543.22},
    {'name': 'FB', 'shares': 200, 'price': 21.09},
    {'name': 'HPQ', 'shares': 35, 'price': 31.75},
    {'name': 'YHOO', 'shares': 45, 'price': 16.35},
    {'name': 'ACME', 'shares': 75, 'price': 115.65}
]
#print(heapq.nlargest(2,list1))
#print(heapq.nsmallest(2,list1))
#print(heapq.nlargest(3,list2,key=lambda x:x['price']))

#itertools模块
"""
迭代工具模块
迭代器的用法不熟
详解：https://docs.python.org/zh-cn/3.7/library/itertools.html
"""

'''
迭代器补充
python中，任意对象，只要定义了__next__方法，它就是一个迭代器。
迭代就是从迭代器中取元素的过程。

如果你不想用for循环迭代呢？这时你可以：

先调用容器（以字符串为例）的iter()函数
再使用 next() 内置函数来调用 __next__() 方法
当元素用尽时，__next__() 将引发 StopIteration 异常
>>> s = 'abc' 
>>> it = iter(s)
>>> it
<iterator object at 0x00A1DB50>
>>> next(it)
'a'
>>> next(it)
'b'
>>> next(it)
'c'

生成器也是一种迭代器，但是你只能对其迭代一次。这是因为它们并没有把所有的值存在内存中，而是在运行时生成值。
大多数时候生成器是以函数来实现的。然而，它们并不返回一个值，而是yield(暂且译作“生出”)一个值。
每次对生成器调用 next() 时，它会从上次离开位置恢复执行（它会记住上次执行语句时的所有数据值）

'''
import itertools

# 产生ABCD的全排列
itertools.permutations('ABCD')
# 产生ABCDE的五选三组合
itertools.combinations('ABCDE', 3)
# 产生ABCD和123的笛卡尔积
itertools.product('ABCD', '123')
# 产生ABC的无限循环序列
itertools.cycle(('A', 'B', 'C'))

#collections模块
'''
namedtuple：命令元组，它是一个类工厂，接受类型的名称和属性列表来创建一个类。
deque：双端队列，是列表的替代实现。Python中的列表底层是基于数组来实现的，而deque底层是双向链表，
    因此当你需要在头尾添加和删除元素是，deque会表现出更好的性能，渐近时间复杂度为$O(1)$。
Counter：dict的子类，键是元素，值是元素的计数，它的most_common()方法可以帮助我们获取出现频率最高的元素。
OrderedDict：dict的子类，它记录了键值对插入的顺序，看起来既有字典的行为，也有链表的行为。
defaultdict：类似于字典类型，但是可以通过默认的工厂函数来获得键对应的默认值，相比字典中的setdefault()方法，这种做法更加高效。
'''

"""
找出序列中出现次数最多的元素
"""
from collections import Counter
words = [
    'look', 'into', 'my', 'eyes', 'look', 'into', 'my', 'eyes',
    'the', 'eyes', 'the', 'eyes', 'the', 'eyes', 'not', 'around',
    'the', 'eyes', "don't", 'look', 'around', 'the', 'eyes',
    'look', 'into', 'my', 'eyes', "you're", 'under'
]
#Counter是一个类，需要是实例化
counter = Counter(words)
#打印第一个，出来的是元组
print(counter.most_common(1))


#数据结构算法

#排序算法
#1.选择排序
def SelectSort(D=[]):
    for i in range(1,len(D)-1):
        #最好使用索引
        min = D[i]
        for j in range(i+1,len(D)):
                if D[i] < min:
                    min,D[i] = D[i],min
    return D
#2.冒泡排序(两两比较相邻的元素)
def BubbleSort(D=[]):
    for i in range(len(D)-1):
        flag = False
        for j in range(i,len(D)-1):
            if D[j+1] < D[j]:
                D[j+1],D[j] = D[j],D[j+1]
                flag = True
            if flag == False:
                break
    return D
#3.合并(将两个有序的列表合并成一个有序的列表)
def Merge(D1=[],D2=[]):
    D_temp = []
    len_D1 = len(D1)
    len_D2 = len(D2)
    i,j=0,0
    while i < len_D1 or j < len_D2:
        if(D1[i] > D2[j]):
            D_temp.append(D2[j])
            j = j+1
        else:
            D_temp.append(D1[i])
            i = i+1
    
    D_temp += D1[i:]
    D_temp += D2[j:]
    return D_temp

#先彻底切分，在一一合并
def MergeSort(D=[]):
    if len(D) < 2:
        return D
    mid = len(D) // 2
    right = MergeSort(D[mid:])
    left = MergeSort(D[:mid])
    return Merge(right,left)

#相当于接口
def HMergeSort(D=[]):
    return MergeSort(D)

#4.折半查找
def Half_Search(D=[],key=0):
    high = len(D)
    low = 0
    while low < high:
        mid = (low + high) // 2
        if D[mid] == key:
            return mid
        elif D[mid] > key:
            high = mid - 1
        else:
            low = mid + 1
    return -1

'''
常用算法；
穷举法、贪婪法、分治法、回溯法、动态规划
'''

#1.
# 公鸡5元一只 母鸡3元一只 小鸡1元三只
# 用100元买100只鸡 问公鸡/母鸡/小鸡各多少只
def Answer_1(money = 0,count = 0):
    for x in range(20):
        for y in range(20):
            z = count - x - y
            if 5*x + 3*y + z == money:
                print('公鸡数量%d,母鸡数量%d，小鸡数量%d' % (x,y,z))
                return x,y,z
    
#2.
# A、B、C、D、E五人在某天夜里合伙捕鱼 最后疲惫不堪各自睡觉
# 第二天A第一个醒来 他将鱼分为5份 扔掉多余的1条 拿走自己的一份
# B第二个醒来 也将鱼分为5份 扔掉多余的1条 拿走自己的一份
# 然后C、D、E依次醒来也按同样的方式分鱼 问他们至少捕了多少条鱼
def Answer_2():
    sum = 0
    start = 1
    for _ in range(5):
        start = start * 5 #有同等5份
        start += 1 #加会扔掉的
    sum=start
    print(sum)
    return sum

#3
#小偷偷东西
#每一步都是最好的，但全局不一定是最好的
class Thing(object):
    def __init__(self,name,price,weight):
        self._name = name
        self._price = price
        self._weight = weight

    @property
    def value(self):
        return '价格重量比:%.2f' % (self._price / self._weight)

#输入的信息
def input_info():
    #默认空格为间隔
    name_str,price_str,weight_str = input().split()
    return name_str,int(price_str),int(weight_str)

#物品实例化
#逻辑一样，编程手法不行，很多逻辑还停留于C语言
def Answer_3():
    #3件物品
    thing = []
    sum_weight = 0
    sum_price = 0
    for i in range(3):
        thing[i].append(Thing(*input_info()))

    #排序
    thing.sort(key = lambda x : x.value,reverse = True)
    for t in thing:
        if sum_weight + thing._weight <= 20:
            sum_weight += thing._weight
            sum_price += thing._price
        
    print(f'小偷偷走了{sum_price}美元的东西')

#4.快速排序
# 一个划分操作，一个替换操作
def QuickSort(D=[]):
    #先定义其枢纽值
    if len(D) > 2:
        pivotpos = Partition(D)
        QuickSort(D[:pivotpos])
        QuickSort(D[pivotpos+1:])
    return D

def Partition(D=[]):
    #找枢纽值
    pivot = D[0]
    right = len(D)-1
    left = 0
    while left < right:
        while left < right and D[right] > pivot:
            right -= 1
        D[left] = D[right]
        while right > left and D[left] < pivot:
            left += 1
        D[right] = D[left]
    D[left] = pivot
    #left=right
    return left
    
#5.
#骑士巡逻
#走一步回看一步
#马走日，走完所有棋盘格子，且不重复
#待解

total = 0
def print_chess(board):
    #board是二维矩阵
    for row in board:
        for col in row:
            print(str(col).center(4), end='')
        

def Answer_5(board,row,col,step=1):
    #边界条件
    if row >=0 and row <8 and col>=0 and col < 8\
        and board[row][col] ==0:
        board[row][col] = step
        #如果走完了
        if step == 8 * 8:
            print_chess(board)
        #递归不撞南墙不回头
        Answer_5(board,row+1,col+2,step+1)
        Answer_5(board,row-1,col+2,step+1)
        Answer_5(board,row+1,col-2,step+1)
        Answer_5(board,row-1,col-2,step+1)
        Answer_5(board,row+2,col+1,step+1)
        Answer_5(board,row-2,col-1,step+1)
        Answer_5(board,row-2,col+1,step+1)
        Answer_5(board,row+2,col-1,step+1)
        board[row][col]=0

'''
def main():
    board = [[0] * 8 for _ in range(8)]
    Answer_5(board,7,7)
'''
if __name__ == '__main__':
    main()