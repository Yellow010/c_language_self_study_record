#动态规划

#1.子列表元素之和的最大值。
'''
知识点补充
map() 会根据提供的函数对指定序列做映射。

第一个参数 function 以参数序列中的每一个元素调用 function 函数
返回包含每次 function 函数返回值的新列表
>>> list(map(lambda x: x ** 2, [1, 2, 3, 4, 5]))   # 使用 lambda 匿名函数
[1, 4, 9, 16, 25]
'''

def Answer_1():
    items = list(map(int,input().split()))
    overall = partial = items[0]
    for i in range(1,len(items)):
        #如果相加的和比下一个值都小，还不如直接从这个值开始
        partial = max(items[i],partial + items[i])
        overall = max(partial,overall)
    print('第一问的答案是：%d' % overall)
    return overall
    
    
def main():
    #Answer_1()
    return


#函数的使用方式
'''
将函数视为“一等公民”

函数可以赋值给变量
函数可以作为函数的参数
函数可以作为函数的返回值

filter() 函数用于过滤序列，过滤掉不符合条件的元素，返回由符合条件元素组成的新列表。

过滤出1~100中平方根是整数的数
newlist = filter(is_sqr, range(1, 101))
print(newlist)
'''
#默认参数：必须参数在前，默认参数在后，默认参数必须指向不变对象！L=None 不能 L=[]
#可变参数：比确定函数的参数是可以在参数前加一个*，表示可变参数，函数内部会把可变参数理解成元组
#         如sum(*numbers),调用sum(1,2,3) 
#         如果numbers已经是一个list或者tuple了，就这样调用sum(*numbers)
#关键字参数：关键字参数允许你传入0个或任意个含参数名的参数，这些关键字参数在函数内部自动组装为一个dict
#          如：def person(name, age, **kw):
#                print('name:', name, 'age:', age, 'other:', kw)
#             >>> person('Bob', 35, city='Beijing')
#                 name: Bob age: 35 other: {'city': 'Beijing'}
#它可以扩展函数的功能。比如，在person函数里，我们保证能接收到name和age这两个参数，但是，如果调用者愿意提供更多的参数，我们也能收到
#如果字典已经存在，引用时就在参数前面加两个**，
#参数的组合
#def creat(a,b,c=0,*args,**kw)

'''
参数的元信息：增加可读性
def add(x:int, y:int) -> int:
    return x + y

lambda表达式典型的使用场景是排序或数据reduce，key=lambda x:x>0
zip([seql, ...])接受一系列可迭代对象作为参数，将对象中对应的元素打包成一个个tuple（元组），然后返回由这些tuples组成的list（列表）
   若传入参数的长度不等，则返回list的长度和参数中长度最短的对象相同
'''

'''
装饰器函数  
他们是修改其他函数的功能的函数，在函数里面定义函数
'''
def father(*name):
    print('你在爸爸这里')

    def Bro():
        return '你在哥哥这里'

    print(Bro())
    print('你又在爸爸这里')

#调用father时Bro才能被调用，Bro不能被外界访问
#函数返回函数
def father2(name):
    def temp():
        return '你在爸爸这里'

    def bro():
        return '你在哥哥这里'

    if name == 'father':
        return temp
    else:
        return bro

#a=father2('fathe')
#a变成了对象
#print(a)
#print(a())

#函数作为参数传递，并修改函数的功能
def new(old_fun):

    def wrapfun():
        print('boring')

        old_fun()

        print('interest')

    return wrapfun

def old_fun():
    print('I am old')

#old_fun()
#重新将old_fun对象化
#old_fun = new(old_fun)
#old_fun()

#使用@来生成装饰器就是old_fun = new(old_fun)的简化
#new_son 把old_fun替代了并对象化了
@new
def new_son():
    print('son')

#new_son()
#print(new_son.__name__)

#更加完整的演示
from functools import wraps
 
def a_new_decorator(a_func):
    @wraps(a_func)
    def wrapTheFunction():
        print("I am doing some boring work before executing a_func()")
        a_func()
        print("I am doing some boring work after executing a_func()")
    return wrapTheFunction

@a_new_decorator
def a_function_requiring_decoration():
    """Hey yo! Decorate me!"""
    print("I am the function which needs some decoration to "
          "remove my foul smell")
 
#print(a_function_requiring_decoration.__name__)


#蓝本典范

def decorator_time(f):

    @wraps(f)
    #我要对f重写
    def decorated(*args,**kw):
        if not can_run:
            return '客官不可以'
        return f(*args,**kw)
    return decorated

@decorator_time
def f():
    return '我又行了'


can_run = False
print(f())

can_run = True
print(f())


#通过类来定义装饰器
#可便于管理与记录
from time import time

class Record(object):
    def __init__(self,output):
        self.output = output

    def __call__(self,fun):
        @wraps(fun)
        def wrapper(*args,**kw):
            start = time()
            result = fun(*args,**kw)
            self.output(fun.__name__,time() - start)
            return result
        return wrapper

def output(name,*args):
    print (name + 'args')

@Record(output)
def func():
    pass 
#print(func())


#python中的闭包相当于c语言中的static，每次运行不会被销毁掉其之前的值
#在使用闭包时，最好使用形参给函数传递值

#单例模式,某个类只出现一个实例
#用装饰器实现单例，就是类的对象作为参数传递给装饰器

def singleton(cls):
    instance = {} #闭包

    @wraps(cls)
    def isin(*args,**kwargs):
        if cls not in instance:
            instance[cls] = cls(*args,**kwargs)

    return isin

@singleton
class President(object):
    def __init__(self, name, age):
          self._name = name
          self._age = age


#实例化参数
president = President('trump',55)
#print(id(president))
president2 = President('bai',66)
#print(id(president2))

#用闭包需要考虑线程的安全性，引用锁
from threading import RLock

def singleton(cls):
    instance = {} #闭包
    locker = RLock()

    @wraps(cls)
    def isin(*args,**kwargs):
        if cls not in instance:
            with locker:
                if cls not in instance:
                    instance[cls] = cls(*args,**kwargs)

    return isin
#先判断一次，再判断一次，性能更好


#面向对象基础：封装、继承、多态

'''
工厂模式:
顾名思义就是我们可以通过一个指定的“工厂”获得需要的“产品”，
在设计模式中主要用于抽象对象的创建过程，让用户可以指定自己想要的对象而不必关心对象的实例化过程。
'''

"""
月薪结算系统 - 部门经理每月15000 程序员每小时200 销售员1800底薪加销售额5%提成
"""

from abc import ABCMeta, abstractmethod

class People(metaclass=ABCMeta):
    "员工的抽象类"

    def __init__(self,name):
        self.name = name

    @abstractmethod
    def get_money(self):
        pass

#员工继承
class Manager(People):
    def __init__(self,name):
        super().__init__(name)

    #继承改变方法
    def get_money(self):
        return 15000
        
class Programmer(People):
    def __init__(self, name ,hours=0.0):
        super().__init__(name)
        self.hours = hours
    
    def get_money(self):
        return 200 * self.hours

class Salesman(People):
    def __init__(self, name,totlemoney=0.0):
        super().__init__(name)
        self.totlemoney = totlemoney

    def get_money(self):
        return 1800 + self.totlemoney * 0.05

#创建工厂接口，生成产品
class PeopleFactory:
    
    @staticmethod
    def creat(type,*args,**kwargs):
        all_type = {'M':Manager,'P':Programmer,'S':Salesman}
        cls = all_type[type.upper()]                        #cls是类的对象

        return cls(*args,**kwargs) if cls else None         #参数长度是不一样的

def main():
    emp = [
        PeopleFactory.creat('M','张三'),
        PeopleFactory.creat('P','李四',50),
        PeopleFactory.creat('S','王五',20000)
        ]

    for t in emp:
        print(f'{t.name}的月工资是：{t.get_money():0.2f}')


'''
类之间的关系：
依赖关系：将一个类的类名或对象当做参数传递给另一个函数被使用的关系就是依赖关系

组合关系：将一个类的对象封装到另一个类的对象的属性中，就叫组合
'''

'''
python对象的复制

引用： 使用引用时，复制的对象和被复制的对象完全相同，修改复制的对象和被复制的对象时，两个都会同时被改变。（比如赋值）
浅复制：使用copy函数，被复制的对象与原对象是相对独立的两个个体，子对象改变父对象不会改变，父对象改变，子对象会改变
深复制：使用copy模块的deepcopy函数，两个对象完全独立
'''

'''
魔法属性：
1.__dict__:类的__dict__属性存储了类定义的所有类属性、类方法等组成的键值对，但不包括继承而来的属性和方法
实例的__dict__属性存储了所有的实例属性的键值对，如果没有就为空；__init__方法其实就是对__dict__属性的初始化赋值

2.__doc__:该属性记录了类的说明文档，用类和实例引用指向的都是类的__doc__属性,如果没有默认为None。

3.__module__:该属性记录类定义的位置，如果定义的位置正好是主程序，那么该值为"_main_",否则是类属于的模块的名字

4.__class__:该属性指向该实例的类，即实例指向类对象，类对象指向元类

5.__slots__:__slots__属性定义好后，限制了一个类的实例的属性以及可以动态添加的属性和方法,__slots__属性定义好后，不得在类中定义元组中已有的同名的方法

魔法方法：
1.__new__:该方法是类创建实例调用的第一个方法，返回一个实例；这是一个实例从无到有必须调用的方法，在单例模式中常用，其他不常用。

2.__init__；

3.__del__；
'''

'''
混入mixin：
class SetOnceMappingMixin:
    """自定义混入类"""
    __slots__ = ()

    def __setitem__(self, key, value):
        if key in self:
            raise KeyError(str(key) + ' already set')
        return super().__setitem__(key, value)


class SetOnceDict(SetOnceMappingMixin, dict):
    """自定义字典"""
    pass


my_dict= SetOnceDict()
try:
    my_dict['username'] = 'jackfrued'
    my_dict['username'] = 'hellokitty'
except KeyError:
    pass
print(my_dict)
'''

'''
元编程和元类
对象是通过类创建的，类是通过元类创建的，元类提供了创建类的元信息。
所有的类都直接或间接的继承自object，所有的元类都直接或间接的继承自type
'''






#if __name__ == '__main__':
#    main() 