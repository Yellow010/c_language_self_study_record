#文件和异常
#open函数
#文件异常的操作总结
#https://segmentfault.com/a/1190000007736783
'''
'r'	读取 （默认）
'w'	写入（会先截断之前的内容，没有文件会自动创建）
'x'	写入，如果文件已经存在会产生异常
'a'	追加，将内容写入到已有文件的末尾
'b'	二进制模式
't'	文本模式（默认）
'+'	更新（既可以读又可以写）
前面的可以与后面的组合
'''


import time 
from math import sqrt
import json
#打开文件
'''
def main():
    #file = None
    #如果文件打开有问题就麻烦了，所以为了程序的健壮性，需要用try
    try:
        with open('D:\\Python_files\\Python_100days\\text.txt','r',encoding='utf-8') as file:
            print(file.readline())
    except FileNotFoundError:
        print('没找到')
    except LookupError:
        print('码没了')
    except UnicodeDecodeError:
        print('码错了')
    
    finally:
        #finally无论程序正常还是异常都会执行
        if file:
            file.close()
    

#使用with open as 读写文件
#可以东调用finally语句关闭文件

'''

'''
#使用readlines按行读取内容
def main():
    #先一次性读全部内容
    with open('D:\\Python_files\\Python_100days\\text.txt','r',encoding='utf-8') as file:
        print(file.read())
        #会自动关闭文件

    with open('D:\\Python_files\\Python_100days\\text.txt','r',encoding='utf-8') as file:
        #逐行读取
        #file.readlines()是一个列表
        ls = file.readlines()
        print(ls)

'''

'''
#将100-999的素数写入三个文件当中

#创建判断素数的函数
def is_prime(n):
    """判断素数的函数"""
    #assert断言函数，当表达式为真则继续进行，为假则抛出异常
    assert n > 0
    for factor in range(2, int(sqrt(n)) + 1):
        if n % factor == 0:
            return False
    return True if n != 1 else False


def main():
    file_names = ('a.txt','b.txt','c.txt')
    file_list = []
    #依次打开文件
    try:
        for file_name in file_names:
            file_list.append(open(file_name,'w',encoding='utf-8'))
        for i in range(1,10000):
            if is_prime(i):
                if i < 100:
                    file_list[0].write(str(i) + '\n')
                elif i < 1000:
                    file_list[1].write(str(i) + '\n')
                else:
                    file_list[2].write(str(i) + '\n')
    except IOError as ex:
        print(ex)
        print('写入失败')
    finally:
        #关闭每一个
        for file in file_list:
            file.close()
            
'''

#读写二进制文件（例如图片）

#复制图片
#读取存储写入完事
#顺便改了一下格式

'''
def main():
    try:
        with open('2077.webp','rb') as f1:
            #存
            data = f1.read()
            print(type(data))
        #复制
        with open('2077(2).jpg','wb') as f2:
            f2.write(data)
    except FileNotFoundError:
        print('母鸡啊')
    except IOError as e:
        print('读写是错了')
'''

#现在我想存储列表或者字典的数据（形式不变）
#需要用到JSON格式来存储
#我们使用json模块就可以将字典或列表以JSON格式保存到文件中

'''
四个重要的方法
dump - 将Python对象按照JSON格式序列化到文件中
dumps - 将Python对象处理成JSON格式的字符串
load - 将文件中的JSON数据反序列化成对象
loads - 将字符串的内容反序列化成Python对象
'''

'''
def main():
    #字典数据
    my_dict = {
        'name': '骆昊',
        'age': 38,
        'qq': 957658,
        'friends': ['王大锤', '白元芳'],
        'cars': [
            {'brand': 'BYD', 'max_speed': 180},
            {'brand': 'Audi', 'max_speed': 280},
            {'brand': 'Benz', 'max_speed': 320}
        ]
    }
    try:
        #创建文件
        with open('data.json','w',encoding='utf-8') as file:
            #格式化
            json.dump(my_dict,file)
    except IOError as e:
        print(e)
    print('数据保存才能')
'''

if __name__ == '__main__':
    main()
