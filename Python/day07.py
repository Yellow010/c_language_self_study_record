#字符串和常用数据结构

"""
s = '\'HAH\tAHA\\'
s1 = r'\'HAH\tAHA\\'
s2 = s * 2
s3 = s1 + s
print(s.find('AH'))
print(s.center(30,'+'))
print(s.isalpha())

#格式化字符串输出方式
a,b = 1,2
print('{0} + {1} = {2}'.format(a,b,a+b))
print(f'{a} + {b} = {a+b}')
"""

#列表
list1 = [2,3,6,4]
#print(list1)
list2 = ['Ha'] * 3
#print(list2)
#print(list1[1],list1[-1])

#for string in list1:
    #print(string)

#for index, elem in enumerate(list1):
    #print(index, elem)

"""
list1.append(7)
list2.append('La')
list1.insert(1,9)

if 9 in list1:
    list1.remove(9)
print(list1)
print(list1.pop(0))
#list1.clear()
#print(list1)

list3 = sorted(list2)
list1.sort(reverse=True)
print(list3)
print(list1)
"""
import sys
"""
f = [x+y for x in '12345' for y in 'ASDFG']
print(sys.getsizeof(f))
#print(f)
g = [x for x in range(1,10)]
# 每次需要数据的时候就通过内部的运算得到数据(需要花费额外的时间)
h = (x for x in range(1,10))
print(sys.getsizeof(g))
print(sys.getsizeof(h))
print(g)
for val in h:
    print(val,end='')
"""

#元组（不可修改）
#更安全，体积更小
'''
T = ('list',332,'我')
print(T[2])
L = list(T) #转化
print(L)
k = tuple(L)
print(k)
'''

#集合(集合运算)
'''
set1 = {1,2,3,4,5}
set2 = set(range(1,5))
set3 = {x for x in range(1,10) if x % 2 == 0}
set3.add(9)
set3.update([11])
print(set1)
print(set2)
print(set3)
print(set2 & set3)
'''

#字典
'''
score = {'1':2,'w':3}
#dict and zip
item = dict(one=1,two=2)
item1 = dict(zip(['one','three'],'23'))
#print(score,item,item1)
#通过建获得值
print(score['1'])
score.update(魏来='zhu')
print(score)
score.popitem()
print(score)
score.clear()
'''

'''
#跑马灯
import os
import time

def main():
    content = '魏来是SB'
    while True:
        # 清理屏幕上的输出
        os.system('cls')  # os.system('clear')
        print(content)
        # 休眠200毫秒
        time.sleep(0.2)
        content = content[1:] + content[0]


if __name__ == '__main__':
    main()
'''

#设计一个函数产生指定长度的验证码，验证码由大小写字母和数字构成
import random

'''
def creatify(code_len = 6):
    #验证码长度为6位
    all_chars = '123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM'
    length_chars = len(all_chars) - 1
    code = ''
    for _ in range(code_len):
        x = random.randint(0,length_chars)
        code += all_chars[x]
    print(code)
    return code

creatify()
'''

#设计一个函数返回给定文件名的后缀名
'''
找到点后面的字符串
'''

'''
def suffix(filename):
    #找点
    pos = filename.find('.')
    print(filename[pos+1:])
    return pos
filename = 'baidu.com'
suffix(filename)
'''

#设计一个函数返回传入的列表中最大和第二大的元素的值。
'''
一种是全遍历两次
先找最大的值，去掉再找一遍最大值
或者一次性比较
'''

'''
def Findgreat(L):
    length = len(L)
    max = 0
    for i in range(1,length):
        if L[i] > L[max]:
            max = i
    print('最大值是%d',L[max])
    L.pop(max)
    max = 0
    for j in range(1,length-1):
        if L[j] > L[max]:
            max = j
    print('第二大的值是%d' ,L[max])
    return

L = [1,6,4,5,1,2,9,7,55,64]
Findgreat(L)
'''

#计算指定的年月日是这一年的第几天
'''
字典
判断是否是闰年再把2月的日子数给替换掉
列表
'''
'''
def day_of_year(year,month,day):
    leap_year = False
    total_day = 0
    day_of_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    #判断是否是闰年
    if  year % 4 == 0 and year % 100 != 0 or year % 400 == 0:
        leap_year = True
        day_of_month[1] = 29
    for x in range(0,month - 1):
        total_day += day_of_month[x]
    total_day += day
    print(total_day)
    return total_day

day_of_year(1998,3,5)
day_of_year(2020,1,17)
'''


#井字棋
'''
无法判断输赢
'''
import os


def print_board(board):
    print(board['TL'] + '|' + board['TM'] + '|' + board['TR'])
    print('-+-+-')
    print(board['ML'] + '|' + board['MM'] + '|' + board['MR'])
    print('-+-+-')
    print(board['BL'] + '|' + board['BM'] + '|' + board['BR'])


def main():
    init_board = {
        'TL': ' ', 'TM': ' ', 'TR': ' ',
        'ML': ' ', 'MM': ' ', 'MR': ' ',
        'BL': ' ', 'BM': ' ', 'BR': ' '
    }
    begin = True
    while begin:
        curr_board = init_board.copy()
        begin = False
        turn = 'x'
        counter = 0
        os.system('clear')
        print_board(curr_board)
        while counter < 9:
            move = input('轮到%s走棋, 请输入位置: ' % turn)
            if curr_board[move] == ' ':
                counter += 1
                curr_board[move] = turn
                if turn == 'x':
                    turn = 'o'
                else:
                    turn = 'x'
            os.system('clear')
            print_board(curr_board)
        choice = input('再玩一局?(yes|no)')
        begin = choice == 'yes'


if __name__ == '__main__':
    main()



